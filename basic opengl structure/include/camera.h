#ifndef ALV_CAMERA
#define ALV_CAMERA

#include "shader.h"
#include <glm/glm.hpp>
#include <glm/gtx/string_cast.hpp>
#include <SDL2/SDL.h>

namespace alv{

    class Camera{
        public:
        glm::vec3 camPos;
        glm::vec3 camFront;
        glm::vec3 camUp;
        glm::mat4 view;
        float speed;
        float sensitivity;
        float pitch;
        float yaw;
        bool looking_around;
        bool first_mouse;
        int currentX, currentY;
        int lastX, lastY; 

        Camera(glm::vec3 position);
        void handleRotation(SDL_Window* window);
        void sendViewMat(alv::Shader* shader);
    };
}
#endif 