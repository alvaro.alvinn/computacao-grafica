#ifndef ALV_ENGINE
#define ALV_ENGINE

#include <GL/glew.h>
#include <GL/gl.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <SDL2/SDL_video.h>
#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <iostream>
#include <time.h>
#include <cmath>

#include "structures.h"
#include "shader.h"
#include "object.h"
#include "ui.h"
#include "camera.h"
#include "utils.h"

typedef int32_t i32;
typedef uint32_t u32;
typedef int32_t b32;

#define WIDTH 800
#define HEIGHT 600

namespace alv{

    class Engine{

        public:
        static Engine* engine;
        //Singelton stuff
        Engine(Engine &other) = delete;
        void operator=(const Engine &) = delete;
        static Engine* GetInstance();

        // The window we'll be rendering to
        SDL_Window *gWindow;
        // WIndow falg
        u32 WindowFlags;

        SDL_GLContext gContext;

        // Event handler
        SDL_Event e;
        SDL_Event last_e;

        // Event Variables
        bool quit;

        GLuint gProgramID;

        bool gRenderQuad = true;

        Camera* cam;

        //time
        float cTime;
        float dTime;
        float lTime;

        uint no_texture;

        std::vector<Object*> objects;
        std::vector<DotLight*> p_light;

        Shader* mainShader;
        Shader* lightShader;

        bool init();
        void setup();
        void simulate();
        void render();
        void handleEvents();
        void exit();

        private:
        Engine();
        void printProgramLog(GLuint program);
        void printShaderLog(GLuint shader);
    };
}
#endif 