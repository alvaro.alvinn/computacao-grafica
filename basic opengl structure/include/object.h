#pragma once 

#include "shader.h"
#include "stb_image.h"
#include "utils.h"
#ifndef OBJECT_H
#define OBEJCT_H

#include <GL/glew.h>
#include <GL/gl.h>
  
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include <algorithm>

#include <glm/ext.hpp>
#include <glm/gtx/string_cast.hpp>

namespace alv{

    enum class TypeEnum{
        NO_COLOR,
        COLORED,
        TEXTURED,
        COLORED_TEXTURED,
        COLORED_NORMAL,
        NORMAL,
        NORMAL_TEXTURED,
        OBJ
    };

    class Object{
        private:
            float * vertexs;
            uint vertexs_count;
            uint * indexs;
            uint indexs_count;

            float * vertexs_positions;
            float * vertexs_colors;
            float * vertexs_normals;
            float * texturecoords;
            
            uint texture;
            uint specular_texture;
            alv::Shader * shader;
            uint VBO;
            uint VAO;
            uint EBO;

            glm::mat4 tMat;
            glm::mat4 rMat;
            glm::mat4 sMat;

            glm::mat4 objMatrix;
            uint materials_count;
            Material * material;
        public:

        glm::vec3 position;

        Object();
        Object(float vertexs[], uint vertexs_count, uint indexs[], uint indexs_count, alv::Shader * shader);
        Object(float vertexs[], uint vertexs_count, uint indexs[], uint indexs_count);
        void loadOBJ(char * file_name);
        void loadMTL(const char * file_name);
        void setup(TypeEnum type);
        void setTexture(uint texture);
        void setSpecularTexture(uint texture);
        void setShader(alv::Shader* shader);
        void draw();
        void relativeTranslate(glm::vec3 target);
        void relativeRotate(glm::vec3 xyz, float angle);
        void relativeScale(glm::vec3 xyz);
        void translate(glm::vec3 target);
        void rotate(glm::vec3 xyz, float angle);
        void scale(glm::vec3 xyz);
        
    };
}


#endif