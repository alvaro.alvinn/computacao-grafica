#pragma once 

#ifndef SHADER_H
#define SHADER_H

#include <GL/glew.h>
#include <GL/gl.h>
  
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

#include <glm/ext.hpp>

#include "structures.h"

namespace alv{
    class Shader
    {
    public:
        // the program ID
        unsigned int ID;
        uint lightsUsed;
        uint lightsAvaliable;
    
        // constructor reads and builds the shader
        Shader();

        // copy constructor
        Shader(const Shader & s);
        // move constructor
        Shader(Shader && s);
        Shader& operator=(const Shader & s);
        Shader& operator=(Shader && s);

        Shader(const char* vertexPath, const char* fragmentPath);
        // use/activate the shader
        void use();
        // utility uniform functions
        void setBool(const std::string &name, bool value) const;  
        void setInt(const std::string &name, int value) const;   
        void setFloat(const std::string &name, float value) const;
        void setMat4(const std::string &name, glm::mat4 value) const;
        void setVec3(const std::string &name, glm::vec3 value) const;

        int addPointLight(alv::DotLight* light);
        int setDirectionalLight(alv::DirectionLight* light);
    };

}
#endif