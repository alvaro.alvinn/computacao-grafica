#pragma once

#include <glm/ext.hpp>
namespace alv{

    typedef struct{
        char * name;
        glm::vec3 ambient_color;
        glm::vec3 diffuse_color;
        glm::vec3 specular_color;
        float shininess;
        uint diffuse_texture;
        uint specular_texture;
    } Material;

    struct DirectionLight{
        glm::vec3 direction;
        glm::vec3 diffuse;
        glm::vec3 specular;
    };

    struct DotLight{
        glm::vec3 position;
        glm::vec3 diffuse;
        glm::vec3 specular;
        float constant;
        float linear;
        float quadratic;
    };
}