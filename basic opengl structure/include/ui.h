#ifndef ALV_UI
#define ALV_UI


#include <GL/glew.h>
#include <GL/gl.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <SDL2/SDL_video.h>
#include <assert.h>
#include <glm/fwd.hpp>
#include <stdint.h>
#include <stdio.h>
#include <vector>
#include <stack>
#include "object.h"


namespace alv{
    // abstract class with the basics components of an UIelement
    class  UIelement {
        protected:
            // object contain the element for, with the vertexes, shades and textures
            Object* object;
            // relative position to its parent element
            glm::vec2 relativePosition;
            // width in pixels
            float width;
            // heigth in pixels
            float heigth;
        public:
            // called from the UIcore, is used to determine the element rendering order
            virtual void addTostack(std::stack<UIelement*>* stack) = 0;
            // acctualy render the element
            virtual void render() = 0;

    };

    class UIcore {
        private:
            // amount of elements the UICore have
            uint elementsCount;
            // elements the UICore have
            std::vector<UIelement*> element;
            // stack that determine the order the elements are rendered
            std::stack<UIelement*> renderStack;
        public:
            UIcore();
            void add(UIelement* element);
            void render();
    };

    class UIbox : public UIelement {
        private:
            // amount of elements childs the UIbox have
            uint childsCount;
            // elements childs the UIbox have
            std::vector<UIelement*> child;
            // elements main color
            glm::vec4 color;
        public:
            UIbox();
            void add(UIelement* element);
            // used by UICore
            void addTostack(std::stack<UIelement*>* stack);
            void render();
    };

    class UItext : public UIelement {
        private:
            char* text;
            glm::vec4 color;
            int size;
            //font
        public:
            UItext(char* text);
            UItext(char* text, glm::vec4 color);
            // used by UICore
            void addTostack(std::stack<UIelement*>* stack);            
            void render();
    };

    class UIloadingBar : public UIelement {
        private:
            float porcentage;
            float speed;
            glm::vec4 color1;
            glm::vec4 color2;
        public:
            UIloadingBar();
            // used by UICore
            void addTostack(std::stack<UIelement*>* stack);
            void render();
    };
}

#endif