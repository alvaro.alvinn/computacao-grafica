#version 330 core
out vec4 FragColor;
struct Material {
    sampler2D diffuse;
    sampler2D specular;
    float     shininess;
}; 

struct Light {
    vec3 position;
  
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};


in vec3 ourColor;
in vec3 fragPos;
in vec3 lPosition;
in vec3 Normal;

in vec2 TexCoords;

uniform Material material;
uniform Light light;  

uniform vec3 viewPos;

void main()
{

    // ambiente ligth
    vec3 ambient = light.ambient * vec3(texture(material.diffuse, TexCoords));

    // normal vectors
    vec3 norm = normalize(Normal);

    // light vectors
    vec3 lightDir = normalize(lPosition - fragPos); 

    // light intesity
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = light.diffuse * diff * vec3(texture(material.diffuse, TexCoords));

    // normalize view direction to the fragment
    vec3 viewDir = normalize(viewPos - fragPos);
    vec3 reflectDir = reflect(-lightDir, norm);

    // qunato maior o expoente mais "reflexivo"
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    vec3 specular = light.specular * (spec * vec3(texture(material.specular, TexCoords))) ;  

    vec3 result = ambient + diffuse + specular;
    FragColor = vec4(result, 1.0);
}