#version 330 core
out vec4 fragColor;
vec2 fragCoord = gl_FragCoord.xy;
  
in vec3 ourColor;
in vec2 TexCoord;

uniform sampler2D ourTexture;

uniform vec3  iResolution;           // viewport resolution (in pixels)
uniform float iTime;

// retorna o valor de alpha da mascara da coluna
float collumn(vec2 position, float thickness, float speed){
    return 1.0 - smoothstep(thickness, thickness+0.005, abs(position.x - ((speed + 0.5) - floor(speed))));
}

void main()
{
    // Normalized pixel coordinates (from 0 to 1)
    vec2 uv = fragCoord/iResolution.xy;
    vec2 q = uv - vec2(0.5,0.5);
    
    // define a velocidade maior o divisor, mais devagar
    float speed = iTime/2.0;

    // define a primera camada com sua cor
    vec3 col = vec3(0.0, 0.74, 0.4);
    vec4 layout1 = vec4(col,1.0);
    // define a segunda camada com sua cor
    vec3 col1 = vec3(0.0,0.93,0.49);
    vec4 layout2 = vec4(col1, 0.0);
    // define a largura de cada coluna
    float t = 0.0225;
    // """insere as colunas""" no layout 2 // 19 numero mágico por enquanto
    for(float i = 0.0; i <= 19.0; i++){
        layout2.a += collumn(q + 0.1*i, t, speed);
    }
    // coloca as camadas
    // Output to screen
    fragColor =  mix(layout1, layout2, layout2.a);
}