#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;
layout (location = 2) in vec2 aTexCoord;
layout (location = 3) in vec3 aNormal;

out vec3 ourColor;
out vec3 lColor;
out vec3 lPosition;
out vec2 TexCoord;
out vec3 fragPos;  
out vec3 Normal;
out vec2 TexCoords;

uniform mat4 modelMat;
uniform mat4 projectionMat;
uniform mat4 viewMat;
uniform vec3 lightPosition;
uniform vec3 Color;

void main()
{
    gl_Position = projectionMat * viewMat * modelMat * vec4(aPos, 1.0);
    fragPos = vec3(modelMat * vec4(aPos, 1.0));
    ourColor = Color;
    TexCoords = aTexCoord;
    lPosition = lightPosition;
    Normal = mat3(transpose(inverse(modelMat))) * aNormal; // passar para CPU muito custoso
    //Normal = aNormal;
}