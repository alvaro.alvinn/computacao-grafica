#include "../include/camera.h"
namespace alv{

    Camera::Camera(glm::vec3 position){
        this->camPos = position;
        this->camUp = glm::vec3(0.0f, 1.0f,  0.0f);
        this->camFront = glm::vec3(0.0f, 0.0f, -1.0f);
        this->speed = 10.0f;
        this->pitch = 1.0f;
        this->yaw = -90.0f;
        this->sensitivity = 0.25f;
        this->looking_around = false;
        this->first_mouse = true; 
    }

    void Camera::handleRotation(SDL_Window* window){
        if(this->looking_around){

            SDL_GetMouseState(&currentX, &currentY);
            

            if (this->first_mouse)
            {
                this->lastX = this->currentX;
                this->lastY = this->currentY;
                this->first_mouse = false;
            
            }

            //std::cout << "lastX: " << this->lastX  << std::endl << "lastY: "<< this->lastY << std::endl << std::endl;

            
            float dX = (float)this->currentX - this->lastX;
            float dY = this->lastY - (float)this->currentY;
            this->lastX = this->currentX;
            this->lastY = this->currentY;

            dX *= this->sensitivity;
            dY *= this->sensitivity;

            this->yaw   += dX;
            this->pitch += dY;  

            if(this->pitch > 89.0f)
                this->pitch =  89.0f;
            if(this->pitch < -89.0f)
                this->pitch = -89.0f;

            //std::cout << "pitch: " << this->pitch  << std::endl << "yaw: "<< this->yaw << std::endl << std::endl;
            
            glm::vec3 direction;
            direction.x = cos(glm::radians(this->yaw)) * cos(glm::radians(this->pitch));
            direction.y = sin(glm::radians(this->pitch));
            direction.z = sin(glm::radians(this->yaw)) * cos(glm::radians(this->pitch));
            this->camFront = glm::normalize(direction);
            SDL_WarpMouseInWindow(window, lastX, lastY);

        }
    };

    void Camera::sendViewMat(alv::Shader* shader){
        this->view = glm::lookAt(this->camPos, this->camPos + this->camFront, this->camUp);
        shader->setMat4("viewMat", this->view);
        //std::cout << glm::to_string(this->view)  << std::endl;
    };
}