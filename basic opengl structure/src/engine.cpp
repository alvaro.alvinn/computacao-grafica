#include "../include/engine.h"

namespace alv {
    Engine* Engine::engine = nullptr;

    Engine::Engine(){

    }

    Engine* Engine::GetInstance(){
        /**
         * This is a safer way to create an instance. instance = new Singleton is
         * dangeruous in case two instance threads wants to access at the same time
         */
        if(engine==nullptr){
            engine = new Engine();
        }
        return engine;
    }

        bool Engine::init(){
            gWindow = NULL;
            WindowFlags = SDL_WINDOW_OPENGL;
            gContext = NULL;

            bool success = true;
            // Initialize SDL

            #ifdef SDL_VIDEO_DRIVER_X11
                /* Always use X11 on platforms that support it, not native Wayland */
                setenv("SDL_VIDEODRIVER", "x11", 1);
            #endif

            if (SDL_Init(SDL_INIT_VIDEO) < 0) {
                printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
                success = false;
            } else {
                // Set texture filtering to linear
                if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1")) {
                printf("Warning: Linear texture filtering not enabled!");
                }
                // Create window
                gWindow =
                    SDL_CreateWindow("Test Opengl", SDL_WINDOWPOS_UNDEFINED,
                                    SDL_WINDOWPOS_UNDEFINED, WIDTH, HEIGHT, WindowFlags);
                if (gWindow == NULL) {
                printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
                success = false;
                } else {
                // Use OpenGL 3.3 core
                SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
                SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
                SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
                                    SDL_GL_CONTEXT_PROFILE_CORE);

                gContext = SDL_GL_CreateContext(gWindow);
                if (gContext == NULL) {
                    printf("OpenGL context could not be created! SDL Error: %s\n",
                        SDL_GetError());
                    success = false;
                } else {
                    // Initialize GLEW
                    glewExperimental = GL_TRUE;
                    GLenum glewError = glewInit();
                    if (glewError != GLEW_OK) {
                    printf("Error initializing GLEW! %s\n",
                            glewGetErrorString(glewError));
                    }
                    else{
                    printf("GLEW initialized!\n");
                    }
                    // Use Vsync
                    if (SDL_GL_SetSwapInterval(1) < 0) {
                    printf("Warning: Unable to set VSync! SDL Error: %s\n",
                            SDL_GetError());
                    }
                    else{
                    printf("Using Vsync!\n");
                    }

                    // Initialize OpenGL
                    gProgramID = glCreateProgram();
                    int nrAttributes;
                    glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &nrAttributes);
                    std::cout << "Maximum nr of vertex attributes supported: " << nrAttributes << std::endl;
                }
                }
            }

            return success;
        }
        
        void Engine::setup(){
            glEnable(GL_DEPTH_TEST);

            mainShader = new alv::Shader("../shaders/vertex.vert", "../shaders/common.frag");

            lightShader = new alv::Shader("../shaders/vertex.vert", "../shaders/ligth.frag");

            cam = new Camera(glm::vec3(0.0f, 0.0f, 3.0f));

            cTime = 0.0f;
            dTime = 0.0f;
            lTime = 0.0f;

            quit = false;

            no_texture = genTexture("../textures/no_texture.jpg");

            p_light.push_back(new DotLight());
            p_light.push_back(new DotLight());
            p_light.push_back(new DotLight());
            p_light.push_back(new DotLight());

            p_light[0]->diffuse    = glm::vec3(0.5f);
            p_light[0]->specular   = glm::vec3(1.0f, 1.0f, 1.0f);
            p_light[0]->constant   = 0.2f;
            p_light[0]->linear     = 0.01f;
            p_light[0]->quadratic  = 0.032f;
            p_light[0]->position   = glm::vec3(2.0f, 6.0f, 4.0f);

            p_light[1]->diffuse    = glm::vec3(0.0f, 1.0f, 0.0f);
            p_light[1]->specular   = glm::vec3(1.0f, 1.0f, 1.0f);
            p_light[1]->constant   = 0.2f;
            p_light[1]->linear     = 0.09f;
            p_light[1]->quadratic  = 0.032f;
            p_light[1]->position   = glm::vec3(-7.0f, 6.0f, 2.0f);

            p_light[2]->diffuse    = glm::vec3(1.0f, 0.0f, 0.0f);
            p_light[2]->specular   = glm::vec3(1.0f, 1.0f, 1.0f);
            p_light[2]->constant   = 0.2f;
            p_light[2]->linear     = 0.09f;
            p_light[2]->quadratic  = 0.032f;
            p_light[2]->position   = glm::vec3(4.0f, 5.0f, 2.0f);

            p_light[3]->diffuse    = glm::vec3(0.5f);
            p_light[3]->specular   = glm::vec3(1.0f, 1.0f, 0.0f);
            p_light[3]->constant   = 0.2f;
            p_light[3]->linear     = 0.09f;
            p_light[3]->quadratic  = 0.032f;
            p_light[3]->position   = glm::vec3(4.2f, 0.0f, -5.0f);
            
            for(int i =0; i<4; i++){

            Object* light = new Object();
            light->loadOBJ("../models/sphere.obj");
            light->setup(alv::TypeEnum::OBJ);
            light->setShader(lightShader);
            light->translate(p_light[i]->position);
            light->scale(glm::vec3(0.2,0.2,0.2));
            objects.push_back(light);
            }

            Object* light = new Object();
            light->loadOBJ("../models/sphere.obj");
            light->setup(alv::TypeEnum::OBJ);
            light->setShader(lightShader);
            light->translate(p_light[0]->position);
            light->scale(glm::vec3(0.2,0.2,0.2));
            objects.push_back(light);

            Object* box = new Object();
            box->loadOBJ("../models/integra.obj");
            box->setup(alv::TypeEnum::OBJ);
            box->setShader(mainShader);

            objects.push_back(box);

            for(int i = 0; i<objects.size(); i++){
                // do something
            }
        }
        
        void Engine::simulate(){
            for(int i = 0; i<objects.size(); i++){
                objects[i]->relativeRotate(glm::vec3(1.0f, 1.0f, 0.0f), 0.05f);
            }
        }
        
        void Engine::render(){
            glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            mainShader->use();
            mainShader->setVec3("Color",  glm::vec3(1.0f, 0.5f, 0.31f));

            mainShader->setVec3("dirLight.diffuse",  glm::vec3(1.0f, 1.0f, 1.0f));
            mainShader->setVec3("dirLight.specular", glm::vec3(1.0f, 1.0f, 1.0f)); 
            mainShader->setVec3("dirLight.direction", glm::vec3(-0.2f, -1.0f, -0.3f));


            mainShader->setVec3("l_ambient",  glm::vec3(0.05f));


            mainShader->addPointLight(p_light[0]);
            mainShader->addPointLight(p_light[1]);
            mainShader->addPointLight(p_light[2]);
            mainShader->addPointLight(p_light[3]);
            

            mainShader->setVec3("viewPos",  cam->camPos);
            cam->sendViewMat(mainShader);
            lightShader->use();
            cam->sendViewMat(lightShader);

            for(int i = 0; i<objects.size(); i++){
                    objects[i]->draw();
            }

            SDL_GL_SwapWindow(gWindow);   
        }
        
        void Engine::handleEvents(){
            cTime = (float)SDL_GetTicks()/1000.0f;
            dTime = cTime - lTime;
            lTime = cTime;
            //gerenciador de eventos
				while (SDL_PollEvent(&e) != 0)
				{
					//sair
					if (e.type == SDL_QUIT)
					{
						quit = true;
					}
					if (e.type == SDL_KEYDOWN){
						// grava o estado de uma flecha
						if(e.key.keysym.sym == SDLK_SPACE){

						}
						if(e.key.keysym.sym == SDLK_ESCAPE){
							quit = true;
						}
                        if(e.key.keysym.sym == SDLK_w){
              cam->camPos += dTime * cam->speed * cam->camFront;
            }

            if(e.key.keysym.sym == SDLK_s){
              cam->camPos -= dTime *cam->speed * cam->camFront;
            }

            if(e.key.keysym.sym == SDLK_a){
              cam->camPos -= glm::normalize(glm::cross(cam->camFront, cam->camUp)) * dTime * cam->speed;

            }

            if(e.key.keysym.sym == SDLK_d){
              cam->camPos += glm::normalize(glm::cross(cam->camFront, cam->camUp)) * dTime * cam->speed;
            }
					}
					if(e.type == SDL_MOUSEBUTTONUP){
            if(e.button.button == SDL_BUTTON_MIDDLE){
              SDL_ShowCursor(SDL_ENABLE);
              cam->looking_around = false;
              cam->first_mouse = true;
            }
					}
					if(e.type == SDL_MOUSEBUTTONDOWN){
            if(e.button.button == SDL_BUTTON_MIDDLE){
              SDL_ShowCursor(SDL_DISABLE);
              cam->looking_around = true;
            }
					}
				}
            cam->handleRotation(gWindow);
        }

        void Engine::exit(){
            SDL_Quit();
            int size = objects.size();
            for(int i = 0; i<size; i++){
                free(objects[i]);
            }
        }

        void Engine::printProgramLog(GLuint program){
            //Make sure name is shader
            if( glIsProgram( program ) )
            {
                //Program log length
                int infoLogLength = 0;
                int maxLength = infoLogLength;
                
                //Get info string length
                glGetProgramiv( program, GL_INFO_LOG_LENGTH, &maxLength );
                
                //Allocate string
                char* infoLog = new char[ maxLength ];
                
                //Get info log
                glGetProgramInfoLog( program, maxLength, &infoLogLength, infoLog );
                if( infoLogLength > 0 )
                {
                    //Print Log
                    printf( "%s\n", infoLog );
                }
                
                //Deallocate string
                delete[] infoLog;
            }
            else
            {
                printf( "Name %d is not a program\n", program );
            }
        }
        
        void Engine::printShaderLog(GLuint shader){
            //Make sure name is shader
            if( glIsShader( shader ) )
            {
                //Shader log length
                int infoLogLength = 0;
                int maxLength = infoLogLength;
                
                //Get info string length
                glGetShaderiv( shader, GL_INFO_LOG_LENGTH, &maxLength );
                
                //Allocate string
                char* infoLog = new char[ maxLength ];
                
                //Get info log
                glGetShaderInfoLog( shader, maxLength, &infoLogLength, infoLog );
                if( infoLogLength > 0 )
                {
                    //Print Log
                    printf( "%s\n", infoLog );
                }

                //Deallocate string
                delete[] infoLog;
            }
            else
            {
                printf( "Name %d is not a shader\n", shader );
            }
        }

}