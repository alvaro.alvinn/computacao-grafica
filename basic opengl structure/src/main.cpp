#include "../include/engine.h"


int main(int ArgCount, char **Args) {
  
  alv::Engine* engine = alv::Engine::GetInstance();
  if(engine->init()){
    engine->setup();

    while(!engine->quit){
      engine->handleEvents();
      engine->simulate();
      engine->render();
    }
      engine->exit();
  }
  return 0;
}