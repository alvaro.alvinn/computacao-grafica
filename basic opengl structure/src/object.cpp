#include "../include/object.h"
namespace alv{

    Object::Object(){
        Object(nullptr, 0, nullptr, 0, nullptr);
    }

    Object::Object(float vertexs[], uint vertexs_count, uint indexs[], uint indexs_count, alv::Shader* shader){
        this->vertexs = (float*) malloc(vertexs_count * sizeof(float));
        this->indexs = (uint*) malloc(indexs_count * sizeof(uint));

        this->vertexs = vertexs;
        this->indexs = indexs;

        this->vertexs_count = vertexs_count;
        this->indexs_count = indexs_count;

        this->shader = shader;

        this->texture = 0;

    }
    Object::Object(float vertexs[], uint vertexs_count, uint indexs[], uint indexs_count){
        this->vertexs = (float*) malloc(vertexs_count * sizeof(float));
        this->indexs = (uint*) malloc(indexs_count * sizeof(uint));

        this->vertexs = vertexs;
        this->indexs = indexs;

        this->vertexs_count = vertexs_count;
        this->indexs_count = indexs_count;
    }
    void Object::setShader(alv::Shader* shader){
        this->shader = shader;
    }
    void Object::draw(){
        this->shader->use();
        this->shader->setInt("material.diffuse", 0);
        this->shader->setInt("material.specular", 1);
        if(this->materials_count == 0){
            this->shader->setFloat("material.shininess", 250.0f);
            this->texture = 1;
            this->specular_texture = 0;
        }
        else{
            this->shader->setFloat("material.shininess", this->material[0].shininess);
            this->texture = this->material[0].diffuse_texture;
            this->specular_texture = this->material[0].specular_texture;
        }
        

        this->shader->setMat4("modelMat", this->objMatrix);
        glActiveTexture(GL_TEXTURE0);    
        glBindTexture(GL_TEXTURE_2D, this->texture);
        glActiveTexture(GL_TEXTURE1);    
        glBindTexture(GL_TEXTURE_2D, this->specular_texture);
        glBindVertexArray(this->VAO);
        glDrawElements(GL_TRIANGLES, indexs_count, GL_UNSIGNED_INT, 0);
    }
    void Object::setTexture(uint texture){
        this->texture = texture;
    }
    void Object::setSpecularTexture(uint texture){
        this->specular_texture = texture;
    }
    void Object::setup(TypeEnum type){

        this->tMat = glm::mat4(1.0f);
        this->rMat = glm::mat4(1.0f);
        this->sMat = glm::mat4(1.0f);
        this->objMatrix = glm::mat4(1.0f);
        this->position = glm::vec3(0.0,0.0,0.0);

        // 1. bind Vertex Array Object
        glGenVertexArrays(1, &VAO);
        glBindVertexArray(VAO);
        // 2. copy our vertices array in a buffer for OpenGL to use
        glGenBuffers(1, &VBO);
        glBindBuffer(GL_ARRAY_BUFFER, VBO); 
        glBufferData(GL_ARRAY_BUFFER, vertexs_count * sizeof(float), vertexs, GL_STATIC_DRAW);
        switch (type)
        {
        case TypeEnum::NO_COLOR:
            // 3. then set our vertex attributes pointers to the sahder layout
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
            glEnableVertexAttribArray(0); 
            break;
        case TypeEnum::COLORED:
            // position attribute
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
            glEnableVertexAttribArray(0);
            // color attribute
            glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
            glEnableVertexAttribArray(1);
            break;
        case TypeEnum::TEXTURED:
            // position attribute
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
            glEnableVertexAttribArray(0);
            // texture coord attribute
            glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
            glEnableVertexAttribArray(2);
            break;
        case TypeEnum::COLORED_TEXTURED:
            // position attribute
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
            glEnableVertexAttribArray(0);
            // color attribute
            glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
            glEnableVertexAttribArray(1);
            // texture coord attribute
            glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
            glEnableVertexAttribArray(2);
            break;
        
        case TypeEnum::COLORED_NORMAL:
            // position attribute
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)0);
            glEnableVertexAttribArray(0);
            // color attribute
            glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)(3 * sizeof(float)));
            glEnableVertexAttribArray(1);
            // normal coord attribute
            glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)(6 * sizeof(float)));
            glEnableVertexAttribArray(3);
            break;

        case TypeEnum::NORMAL:
            // position attribute
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
            glEnableVertexAttribArray(0);
            // normal coord attribute
            glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
            glEnableVertexAttribArray(3);
            break;
        case TypeEnum::NORMAL_TEXTURED:
            // position attribute
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
            glEnableVertexAttribArray(0);
            // normal coord attribute
            glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
            glEnableVertexAttribArray(3);
            // texture coord attribute
            glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
            glEnableVertexAttribArray(2);
            break;

        case TypeEnum::OBJ:
            // position attribute
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
            glEnableVertexAttribArray(0);
            // texture coord attribute
            glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
            glEnableVertexAttribArray(2);
            // normal coord attribute
            glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(5 * sizeof(float)));
            glEnableVertexAttribArray(3);
            break;
        default:
            break;
        }
        glGenBuffers(1, &EBO); 
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexs_count * sizeof(uint), indexs, GL_STATIC_DRAW);
    }
    void Object::relativeTranslate(glm::vec3 target){
        
        this->tMat = glm::translate(this->tMat, target);
        
        this->objMatrix = tMat * sMat * rMat;
        
    }

    void Object::relativeRotate(glm::vec3 xyz, float angle){
        
        this->rMat = glm::rotate(this->rMat, glm::radians(angle), xyz);
        
        this->objMatrix = tMat * sMat * rMat;
        
    }

    void Object::relativeScale(glm::vec3 xyz){
        this->sMat = glm::scale(this->sMat, xyz);
        this->objMatrix = tMat * sMat * rMat;
    }

    void Object::translate(glm::vec3 target){
        this->tMat = glm::mat4(1.0f);
        this->tMat = glm::translate(this->tMat, target);
        this->objMatrix = tMat * sMat * rMat;
    }

    void Object::rotate(glm::vec3 xyz, float angle){
        this->rMat = glm::mat4(1.0f);
        this->rMat = glm::rotate(this->rMat, glm::radians(angle), xyz);
        this->objMatrix = tMat * sMat * rMat;
    }

    void Object::scale(glm::vec3 xyz){
        this->sMat = glm::mat4(1.0f);
        this->sMat = glm::scale( this->sMat, xyz);
        this->objMatrix = tMat * sMat * rMat;
    }

    void Object::loadOBJ(char * file_name){
        std::stringstream ss;
        std::ifstream in_file(file_name);
        std::string line = "";
        std::string prefix ="";

        std::cout << "\nloadig " << file_name << "\n";

        if(!in_file.is_open()){
            throw "ERROR::OBJECT::loadOBJ::Could no read file!";
        }

        std::vector<glm::vec3> vertex_positions;
        std::vector<glm::vec2> vertex_texcoords;
        std::vector<glm::vec3> vertex_normals;

        std::vector<int> vertex_positions_indices;
        std::vector<int> vertex_texcoords_indcies;
        std::vector<int> vertex_normals_indices;

        while(std::getline(in_file, line)){
            ss.clear();
            ss.str(line);
            ss >> prefix;

            if(prefix == "#"){

            }
            else if(prefix == "o"){

            }
            else if(prefix == "s"){

            }
            else if(prefix == "v"){     // vertex positions
                glm::vec3 vertex;
                ss >> vertex.x >> vertex.y >> vertex.z;
                vertex_positions.push_back(vertex);
            }
            else if(prefix == "vt"){    // vertex normals
                glm::vec2 texture_coord;
                ss >> texture_coord.x >> texture_coord.y;
                vertex_texcoords.push_back(texture_coord);
            }
            else if(prefix == "vn"){    // vertex normals
                glm::vec3 normal;
                ss >> normal.x >> normal.y >> normal.z;
                vertex_normals.push_back(normal);
            }
            else if (prefix == "f"){
                int counter = 0;
                int tmp;
                while (ss >> tmp)
                {
                    if(counter == 0)
                        vertex_positions_indices.push_back(tmp);
                    else if(counter == 1)
                        vertex_texcoords_indcies.push_back(tmp);
                    else if(counter == 2)
                        vertex_normals_indices.push_back(tmp);

                    if(ss.peek() == '/'){
                        counter++;
                        ss.ignore(1, '/');
                    }
                    else if(ss.peek() == ' '){
                        counter++;
                        ss.ignore(1, ' ');
                    }

                    if(counter > 2){
                        counter = 0;
                    }
                }

            }
            else{

            }
        }

            /**
             * 
             * p = position (3)+
             * t = texture  (2)+
             * n = normal   (3)+
             *               8
             * p.x p.y p.z  t.x t.y  n.x n.y n.z
             * 
             */

            this->vertexs = (float*) malloc(vertex_positions_indices.size() * sizeof(float) * 8);
            this->vertexs_count = vertex_positions_indices.size() * 8;

            this->indexs_count = vertex_positions_indices.size();
            this->indexs = (uint*) malloc(this->vertexs_count * sizeof(uint));

            for(uint i = 0; i < this->indexs_count; i++){
                this->indexs[i] = i;
            }

            for(uint i = 0; i < vertex_positions_indices.size(); i++){
                this->vertexs[0 + (8*i)] = vertex_positions[vertex_positions_indices[i]-1].x;
                this->vertexs[1 + (8*i)] = vertex_positions[vertex_positions_indices[i]-1].y;
                this->vertexs[2 + (8*i)] = vertex_positions[vertex_positions_indices[i]-1].z;

                //std::cout << "adicionada posicao: " << this->vertexs[0 + (8*i)] << " " << this->vertexs[1 + (8*i)] << " " << this->vertexs[2 + (8*i)] << "\n";

                this->vertexs[3 + (8*i)] = vertex_texcoords[vertex_texcoords_indcies[i]-1].x;
                this->vertexs[4 + (8*i)] = vertex_texcoords[vertex_texcoords_indcies[i]-1].y;

                //std::cout << "adicionada textura: " << this->vertexs[3 + (8*i)] << " " << this->vertexs[3 + (8*i)] << "\n";

                this->vertexs[5 + (8*i)] = vertex_normals[vertex_normals_indices[i]-1].x;
                this->vertexs[6 + (8*i)] = vertex_normals[vertex_normals_indices[i]-1].y;
                this->vertexs[7 + (8*i)] = vertex_normals[vertex_normals_indices[i]-1].z;

                //std::cout << "adicionada normal: " << this->vertexs[5 + (8*i)] << " " << this->vertexs[6 + (8*i)] << " " << this->vertexs[7 + (8*i)] << "\n";
            }


            //debug
            //std::cout << line << '\n';
        
        std::cout << file_name << " loaded:\n";
        std::cout << "\tn of vertices: " << vertex_positions.size() << '\n';
        std::cout << "\tn of texcoords: " << vertex_texcoords.size() << '\n';
        std::cout << "\tn of normals: " << vertex_normals.size() << '\n';

        std::string str = file_name;
        str.pop_back();
        str.pop_back();
        str.pop_back();

        str.push_back('m');
        str.push_back('t');
        str.push_back('l');


        this->loadMTL(str.c_str());

    }

    void Object::loadMTL(const char * file_name){
        std::stringstream ss;
        std::ifstream in_file(file_name);
        std::string line = "";
        std::string prefix ="";

        std::cout << "\nloadig " << file_name << "\n";

        if(!in_file.is_open()){
            std::cerr << "ERROR::OBJECT::loadMTL::Could no read file!" << std::endl;
        }

        std::vector<Material> materials;
        this->materials_count = 0;

        while(std::getline(in_file, line)){
            ss.clear();
            ss.str(line);
            ss >> prefix;

            if(prefix == "#"){

            }
            else if(prefix == "newmtl"){    // material name
                this->materials_count++;
                Material m;
                std::string str;
                ss >> str;
                m.name = (char*)str.c_str();
                m.diffuse_texture = 1;
                m.specular_texture = 0;
                materials.push_back(m);
            }
            else if(prefix == "Ns"){        // specular highlights
                ss >> materials[this->materials_count-1].shininess;
            }
            else if(prefix == "Ka"){        // ambient color
                glm::vec3 aColor;
                ss >> aColor.r >> aColor.g >> aColor.b;
                materials[this->materials_count-1].ambient_color = aColor;
            }
            else if(prefix == "Kd"){        // ambient color
                glm::vec3 dColor;
                ss >> dColor.r >> dColor.g >> dColor.b;
                materials[this->materials_count-1].diffuse_color = dColor;

            }
            else if(prefix == "Ke"){        // emissive (not used)

            }
            else if(prefix == "Ni"){        // optical density (not used)

            }
            else if (prefix == "d"){        // dissolve (not used)

            }
            else if (prefix == "illum"){    //  illumination model (not used)

            }
            else if (prefix == "map_Kd"){   // color texture
                std::string file_name;
                ss >> file_name;
                materials[this->materials_count-1].diffuse_texture = genTexture(file_name.c_str());
            }
            else if (prefix == "map_Ks"){   // specular texture
                std::string file_name;
                ss >> file_name;
                materials[this->materials_count-1].specular_texture = genTexture(file_name.c_str());

            }
            else{

            }
        }
        this->material = (Material*)malloc(this->materials_count * sizeof(Material));
        for(int i = 0; i<this->materials_count; i++){
            this->material[i] = materials[i];
            printf("Material loaded:\n");
            std::cout << "name: " << this->material[i].name << std::endl;
            std::cout << "shininess: " << this->material[i].shininess << std::endl;
            std::cout << "ambient color: " << glm::to_string(this->material[i].ambient_color) << std::endl;
            std::cout << "diffuse color: " << glm::to_string(this->material[i].diffuse_color) << std::endl;
            std::cout << "diffuse_texture: " << this->material[i].diffuse_texture << std::endl;
            std::cout << "specular_texture: " << this->material[i].specular_texture << std::endl;
        }
    }
}
