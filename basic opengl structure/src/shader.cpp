#include "../include/shader.h"
#include <glm/ext/matrix_transform.hpp>
using namespace alv;
Shader::Shader(){
    
}

Shader::Shader(const char* vertexPath, const char* fragmentPath)
{
    // 1. retrieve the vertex/fragment source code from filePath
    std::string vertexCode;
    std::string fragmentCode;
    std::ifstream vShaderFile;
    std::ifstream fShaderFile;
    // ensure ifstream objects can throw exceptions:
    vShaderFile.exceptions (std::ifstream::failbit | std::ifstream::badbit);
    fShaderFile.exceptions (std::ifstream::failbit | std::ifstream::badbit);
    try 
    {
        // open files
        vShaderFile.open(vertexPath);
        fShaderFile.open(fragmentPath);
        std::stringstream vShaderStream, fShaderStream;
        // read file's buffer contents into streams
        vShaderStream << vShaderFile.rdbuf();
        fShaderStream << fShaderFile.rdbuf(); 	
        // close file handlers
        vShaderFile.close();
        fShaderFile.close();
        // convert stream into string
        vertexCode   = vShaderStream.str();
        fragmentCode = fShaderStream.str();		
    }
    catch(std::ifstream::failure &e)
    {
        std::cout << "ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ" << std::endl;
        std::cout << vertexPath << std::endl;
        std::cout << fragmentPath << std::endl;
        
    }
    const char* vShaderCode = vertexCode.c_str();
    const char* fShaderCode = fragmentCode.c_str();

    // 2. compile shaders
    unsigned int vertex, fragment;
    int success;
    char infoLog[512];

    // vertex Shader
    vertex = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex, 1, &vShaderCode, NULL);
    glCompileShader(vertex);
    // print compile errors if any
    glGetShaderiv(vertex, GL_COMPILE_STATUS, &success);
    if(!success)
    {
        glGetShaderInfoLog(vertex, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
    };

    // fragment Shader
    fragment = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment, 1, &fShaderCode, NULL);
    glCompileShader(fragment);
    // print compile errors if any
    glGetShaderiv(fragment, GL_COMPILE_STATUS, &success);
    if(!success)
    {
        glGetShaderInfoLog(fragment, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
    };

    // shader Program
    ID = glCreateProgram();
    glAttachShader(ID, vertex);
    glAttachShader(ID, fragment);
    glLinkProgram(ID);
    // print linking errors if any
    glGetProgramiv(ID, GL_LINK_STATUS, &success);
    if(!success)
    {
        glGetProgramInfoLog(ID, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
    }

    // delete the shaders as they're linked into our program now and no longer necessary
    glDeleteShader(vertex);
    glDeleteShader(fragment);

    glUseProgram(ID);
    glm::mat4 view = glm::mat4(1.0f);
    this->setMat4("viewMat", glm::translate(view, glm::vec3(0.0f, 0.0f, -3.0f)));
    glm::mat4 projection = glm::perspective(glm::radians(45.0f), 800.0f / 600.0f, 0.1f, 100.0f);
    this->setMat4("projectionMat", projection);

    lightsAvaliable = 4;
    lightsUsed = 0;

}

void Shader::use() 
{ 
    glUseProgram(this->ID);
}

void Shader::setBool(const std::string &name, bool value) const
{         
    glUniform1i(glGetUniformLocation(ID, name.c_str()), (int)value); 
}
void Shader::setInt(const std::string &name, int value) const
{ 
    glUniform1i(glGetUniformLocation(ID, name.c_str()), value); 
}
void Shader::setFloat(const std::string &name, float value) const
{ 
    glUniform1f(glGetUniformLocation(ID, name.c_str()), value); 
}
void Shader::setMat4(const std::string &name, glm::mat4 value) const
{ 
    // std::cout << "retorno de glGetUniformLocation(ID, name.c_str()): " << glGetUniformLocation(ID, name.c_str()) << std::endl;
    glUniformMatrix4fv(glGetUniformLocation(ID, name.c_str()), 1, GL_FALSE, glm::value_ptr(value) ); 
}
void Shader::setVec3(const std::string &name, glm::vec3 value) const
{  
    glUniform3fv(glGetUniformLocation(ID, name.c_str()), 1, glm::value_ptr(value));
}

int Shader::addPointLight(alv::DotLight* light){
    if(lightsUsed < lightsAvaliable){
        this->use();
        if(lightsUsed == 0){
            this->setVec3("pointLights[0].diffuse",  light->diffuse);
            this->setVec3("pointLights[0].specular", light->specular); 
            this->setFloat("pointLights[0].constant",  light->constant);
            this->setFloat("pointLights[0].linear",    light->linear );
            this->setFloat("pointLights[0].quadratic", light->quadratic);
            this->setVec3("pointLights[0].position",  light->position);
        }
        if(lightsUsed == 1){
            this->setVec3("pointLights[1].diffuse",  light->diffuse);
            this->setVec3("pointLights[1].specular", light->specular); 
            this->setFloat("pointLights[1].constant",  light->constant);
            this->setFloat("pointLights[1].linear",    light->linear );
            this->setFloat("pointLights[1].quadratic", light->quadratic);
            this->setVec3("pointLights[1].position",  light->position);
        }
        if(lightsUsed == 2){
            this->setVec3("pointLights[2].diffuse",  light->diffuse);
            this->setVec3("pointLights[2].specular", light->specular); 
            this->setFloat("pointLights[2].constant",  light->constant);
            this->setFloat("pointLights[2].linear",    light->linear );
            this->setFloat("pointLights[2].quadratic", light->quadratic);
            this->setVec3("pointLights[2].position",  light->position);
        }
        if(lightsUsed == 3){
            this->setVec3("pointLights[3].diffuse",  light->diffuse);
            this->setVec3("pointLights[3].specular", light->specular); 
            this->setFloat("pointLights[3].constant",  light->constant);
            this->setFloat("pointLights[3].linear",    light->linear );
            this->setFloat("pointLights[3].quadratic", light->quadratic);
            this->setVec3("pointLights[3].position",  light->position);
        }
        lightsUsed++;
        return 1;
    }
    return -1;
}
int Shader::setDirectionalLight(alv::DirectionLight* light){
    this->use();
    this->setVec3("dirLight.diffuse",  light->diffuse);
    this->setVec3("dirLight.specular", light->specular); 
    this->setVec3("dirLight.direction", light->direction);
    return 1;
}


// copy constructor
Shader::Shader(const Shader & s){

    ID = s.ID;

}
// move constructor
Shader::Shader(Shader && s){

    ID = std::move(s.ID);

}
Shader& Shader::operator=(const Shader & s){

    ID = s.ID;
    return *this;
}
Shader& Shader::operator=(Shader && s){

    ID = std::move(s.ID);
    return *this;
}