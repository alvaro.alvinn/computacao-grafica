#include "../include/ui.h"
#include <cstdlib>


namespace alv{

UIcore::UIcore(){
    this->elementsCount = 0;
    this->element = std::vector<UIelement*>();
}
void UIcore::add(UIelement* element){
    this->elementsCount++;
    this->element.push_back(element);
}
void UIcore::render(){
    int i = 1;
    std::cout << "tamanho do vector: " << element.size() << "tamanho guardado: " << elementsCount << std::endl;
    for(int i = 0; i<elementsCount; i++){
        element[i]->addTostack(&renderStack);
        std::cout << "  finalizou stack, iniciando render..." << std::endl;
    }
    while(!renderStack.empty()){
        renderStack.top()->render();
        renderStack.pop();
    }    
}


UIbox::UIbox(){
    this->childsCount = 0;
    this->child = std::vector<UIelement*>();
}
void UIbox::add(UIelement* element){
    this->childsCount++;
    this->child.push_back(element);
}
void UIbox::addTostack(std::stack<UIelement*>* stack){
        for(uint i = 0; i<childsCount; i++){
        child[i]->addTostack(stack);
    }
    stack->push(this);
    std::cout << "  adicionou box à stack" << std::endl;
}
void UIbox::render(){
    // render function
    std::cout << "      renderizou box" << std::endl;
}



UItext::UItext(char* text){
    this->text = text;
    this->color = glm::vec4(1.0,1.0,1.0,1.0);
    this->size = 32;
}
UItext::UItext(char* text, glm::vec4 color){
    this->text = text;
    this->color = color;
    this->size = 32;
}
void UItext::addTostack(std::stack<UIelement*>* stack){
    stack->push(this);
    std::cout << "  adicionou text à stack" << std::endl;
}
void UItext::render(){
    //render code
    std::cout << "      renderizou texto" << std::endl;
}

UIloadingBar::UIloadingBar(){
    this->porcentage = 100.0;
    this->speed = 1.0;
    this->color1 = glm::vec4(0.0, 0.74, 0.4, 1.0);
    this->color2 = glm::vec4(0.0,0.93,0.49, 1.0);
}

void UIloadingBar::addTostack(std::stack<UIelement*>* stack){
    stack->push(this);
    std::cout << "  adicionou barra à stack" << std::endl;
}

void UIloadingBar::render(){
    //render code
    std::cout << "      renderizou barra de carregamento" << std::endl;
}

}