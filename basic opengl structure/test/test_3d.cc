#include <GL/glew.h>
#include <GL/gl.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <SDL2/SDL_video.h>
#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <iostream>
#include <time.h>
#include <cmath>

#include "../include/shader.h"
#include "../include/object.h"
#include "../include/ui.h"
#include "../include/camera.h"

typedef int32_t i32;
typedef uint32_t u32;
typedef int32_t b32;

#define WIDTH 800
#define HEIGHT 600



// The window we'll be rendering to
SDL_Window *gWindow = NULL;
// WIndow falg
u32 WindowFlags = SDL_WINDOW_OPENGL;

SDL_GLContext gContext = NULL;

// Event handler
SDL_Event e;
SDL_Event ultimo_e;

// Event Variables
bool quit = false;

GLuint gProgramID;

GLuint gVBO;
GLuint gIBO;

GLint gVertexPos2DLocation;

bool gRenderQuad = true;

alv::Camera cam(glm::vec3(0.0f, 0.0f, 3.0f));

//time
float cTime = 0.0f;
float dTime = 0.0f;
float lTime = 0.0f;

void printProgramLog( GLuint program )
{
    //Make sure name is shader
    if( glIsProgram( program ) )
    {
        //Program log length
        int infoLogLength = 0;
        int maxLength = infoLogLength;
        
        //Get info string length
        glGetProgramiv( program, GL_INFO_LOG_LENGTH, &maxLength );
        
        //Allocate string
        char* infoLog = new char[ maxLength ];
        
        //Get info log
        glGetProgramInfoLog( program, maxLength, &infoLogLength, infoLog );
        if( infoLogLength > 0 )
        {
            //Print Log
            printf( "%s\n", infoLog );
        }
        
        //Deallocate string
        delete[] infoLog;
    }
    else
    {
        printf( "Name %d is not a program\n", program );
    }
}

void printShaderLog( GLuint shader )
{
    //Make sure name is shader
    if( glIsShader( shader ) )
    {
        //Shader log length
        int infoLogLength = 0;
        int maxLength = infoLogLength;
        
        //Get info string length
        glGetShaderiv( shader, GL_INFO_LOG_LENGTH, &maxLength );
        
        //Allocate string
        char* infoLog = new char[ maxLength ];
        
        //Get info log
        glGetShaderInfoLog( shader, maxLength, &infoLogLength, infoLog );
        if( infoLogLength > 0 )
        {
            //Print Log
            printf( "%s\n", infoLog );
        }

        //Deallocate string
        delete[] infoLog;
    }
    else
    {
        printf( "Name %d is not a shader\n", shader );
    }
}

bool init() {
  bool success = true;
  // Initialize SDL

  #ifdef SDL_VIDEO_DRIVER_X11
    /* Always use X11 on platforms that support it, not native Wayland */
    setenv("SDL_VIDEODRIVER", "x11", 1);
  #endif

  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
    success = false;
  } else {
    // Set texture filtering to linear
    if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1")) {
      printf("Warning: Linear texture filtering not enabled!");
    }

    

    // Create window
    gWindow =
        SDL_CreateWindow("Test Opengl", SDL_WINDOWPOS_UNDEFINED,
                         SDL_WINDOWPOS_UNDEFINED, WIDTH, HEIGHT, WindowFlags);
    if (gWindow == NULL) {
      printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
      success = false;
    } else {
      // Use OpenGL 3.3 core
      SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
      SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
      SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
                          SDL_GL_CONTEXT_PROFILE_CORE);

      gContext = SDL_GL_CreateContext(gWindow);
      if (gContext == NULL) {
        printf("OpenGL context could not be created! SDL Error: %s\n",
               SDL_GetError());
        success = false;
      } else {
        // Initialize GLEW
        glewExperimental = GL_TRUE;
        GLenum glewError = glewInit();
        if (glewError != GLEW_OK) {
          printf("Error initializing GLEW! %s\n",
                 glewGetErrorString(glewError));
        }
        else{
          printf("GLEW initialized!\n");
        }
        // Use Vsync
        if (SDL_GL_SetSwapInterval(1) < 0) {
          printf("Warning: Unable to set VSync! SDL Error: %s\n",
                 SDL_GetError());
        }
        else{
          printf("Using Vsync!\n");
        }

        // Initialize OpenGL
        gProgramID = glCreateProgram();
        int nrAttributes;
        glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &nrAttributes);
        std::cout << "Maximum nr of vertex attributes supported: " << nrAttributes << std::endl;
      }
    }
  }

  return success;
}

void handleEvents(){
  //gerenciador de eventos
				while (SDL_PollEvent(&e) != 0)
				{
					//sair
					if (e.type == SDL_QUIT)
					{
						quit = true;
					}
					if (e.type == SDL_KEYDOWN){
						// grava o estado de uma flecha
						if(e.key.keysym.sym == SDLK_SPACE){

						}
						if(e.key.keysym.sym == SDLK_ESCAPE){
							quit = true;
						}
                        if(e.key.keysym.sym == SDLK_w){
              cam.camPos += dTime * cam.speed * cam.camFront;
            }

            if(e.key.keysym.sym == SDLK_s){
              cam.camPos -= dTime *cam.speed * cam.camFront;
            }

            if(e.key.keysym.sym == SDLK_a){
              cam.camPos -= glm::normalize(glm::cross(cam.camFront, cam.camUp)) * dTime * cam.speed;

            }

            if(e.key.keysym.sym == SDLK_d){
              cam.camPos += glm::normalize(glm::cross(cam.camFront, cam.camUp)) * dTime * cam.speed;
            }
					}
					if(e.type == SDL_MOUSEBUTTONUP){
            if(e.button.button == SDL_BUTTON_MIDDLE){
              SDL_ShowCursor(SDL_ENABLE);
              cam.looking_around = false;
              cam.first_mouse = true;
            }
					}
					if(e.type == SDL_MOUSEBUTTONDOWN){
            if(e.button.button == SDL_BUTTON_MIDDLE){
              SDL_ShowCursor(SDL_DISABLE);
              cam.looking_around = true;
            }
					}
				}
}

uint genTexture(const char* path){
  uint texture;
  glGenTextures(1, &texture);
  glBindTexture(GL_TEXTURE_2D, texture);
  // set the texture wrapping/filtering options (on the currently bound texture object)
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  // load and generate the texture
  int width, height, nrChannels;
  unsigned char *data = stbi_load(path, &width, &height, &nrChannels, 0);
  if (data)
  {
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
      glGenerateMipmap(GL_TEXTURE_2D);
  }
  else
  {
      std::cout << "Failed to load texture" << std::endl;
  }
  stbi_image_free(data);
  return texture;
};

int main(int ArgCount, char **Args) {

  init();

  // VERTICES

float vertices0[] = {
  //  x          y        z
     0.5f,  0.5f, 0.0f,  // top right
     0.25f, -0.5f, 0.0f,  // bottom right l
     0.75f, -0.5f, 0.0f  // bottom right r
     
};
unsigned int indices0[] = {  // note that we start from 0!
    0, 1, 2,   // first triangle
};

    float vertices1[] = {
        // positions          // colors           // texture coords
         0.5f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f, // top right
         0.5f, -0.5f, 0.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f, // bottom right
        -0.5f, -0.5f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f, // bottom left
        -0.5f,  0.5f, 0.0f,   1.0f, 1.0f, 0.0f,   0.0f, 1.0f  // top left 
    };
    unsigned int indices1[] = {
        0, 1, 3, // first triangle
        1, 2, 3  // second triangle
    };

      float vertices2[] = {
        // positions          // colors           // texture coords
         0.5f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f, // top right
         0.5f, -0.5f, 0.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f, // bottom right
        -0.5f, -0.5f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f, // bottom left
        -0.5f,  0.5f, 0.0f,   1.0f, 1.0f, 0.0f,   0.0f, 1.0f  // top left 
    };
    unsigned int indices2[] = {
        0, 1, 3, // first triangle
        1, 2, 3  // second triangle
    };

    float vertices3D[] = {
    -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
     0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
    -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,

    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
     0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
    -0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,

    -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
    -0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
    -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
     0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
     0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
     0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
     0.5f, -0.5f, -0.5f,  1.0f, 1.0f,
     0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
     0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,

    -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
    -0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
    -0.5f,  0.5f, -0.5f,  0.0f, 1.0f
};

    uint indicies3D[] = {
    0, 1, 2, 3, 4, 5, 6, 7, 8,
    9, 10, 11, 12, 13, 14, 15, 16,
    17, 18, 19, 20,  21, 22, 23,
    24, 25, 26,27, 28, 29, 30,
    31, 32, 33, 34, 35
};

  alv::Shader* mainShader = new alv::Shader("../shaders/vertex.vert", "../shaders/loading.frag");

  alv::Shader* textureShader = new alv::Shader("../shaders/vertex.vert", "../shaders/2dTextured.frag");

  alv::Shader* texture2dShader = new alv::Shader("../shaders/vertex.vert", "../shaders/2dTextured.frag");
  uint wallTexture = genTexture("../textures/wall.jpg");

  alv::Object obj0(vertices1, 32, indices1, 6, mainShader);
  obj0.setTexture(wallTexture);
  obj0.setup(alv::TypeEnum::COLORED_TEXTURED);

  obj0.translate(glm::vec3(-1.0f,1.15f, 0));
  obj0.scale(glm::vec3(1.25f, 0.10f, 1.0f));

  alv::Object obj1(vertices2, 32, indices2, 6, texture2dShader);
  obj1.setTexture(wallTexture);
  obj1.setup(alv::TypeEnum::COLORED_TEXTURED);

  obj1.translate(glm::vec3(-1.5f,0.9f, 0));
  obj1.scale(glm::vec3(0.25f,0.25f,1.0f));
  
  alv::Object obj3D(vertices3D, 180, indicies3D, 36, textureShader);
  obj3D.setTexture(wallTexture);
  obj3D.setup(alv::TypeEnum::TEXTURED);

  unsigned int iteration = 0;
  float scale = -0.02;

  glEnable(GL_DEPTH_TEST);  
  

  while(!quit){
  cTime = (float)SDL_GetTicks()/1000.0f;
  dTime = cTime - lTime;
  lTime = cTime;
    handleEvents();
    cam.handleRotation(gWindow);
    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // para o shader de loading
    mainShader->use();
    mainShader->setFloat("iTime", cTime);
    mainShader->setVec3("iResolution", glm::vec3(600,800,0));

    textureShader->use();
    cam.sendViewMat(textureShader);

    //obj0.relativeRotate(glm::vec3(0.0f, 0.0f, 1.0f), 1.0f);
    //obj0.relativeScale(glm::vec3(1.0f + scale, 1.0f + scale, 1.0f));
    //obj1.relativeScale(glm::vec3(1.0f - scale, 1.0f - scale, 1.0f));
    //obj1.relativeRotate(glm::vec3(0.0f, 0.0f, 1.0f), 1.0f);

    obj3D.relativeRotate(glm::vec3(1.0f, 1.0f, 0.0f), 1.0f);

    


    
    obj1.draw();
    obj0.draw();
    obj3D.draw();
    
    SDL_GL_SwapWindow(gWindow);
    
    iteration++;
    if(iteration == 60){
      iteration = 0;
      scale = -scale;
    }
    
  }

  SDL_Quit();
  
  return 0;
}