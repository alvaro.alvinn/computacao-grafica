#include <GL/glew.h>
#include <GL/gl.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <SDL2/SDL_video.h>
#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <iostream>
#include <time.h>
#include <cmath>

#include "../include/ui.h"

typedef int32_t i32;
typedef uint32_t u32;
typedef int32_t b32;

#define WIDTH 800
#define HEIGHT 600



// The window we'll be rendering to
SDL_Window *gWindow = NULL;
// WIndow falg
u32 WindowFlags = SDL_WINDOW_OPENGL;

SDL_GLContext gContext = NULL;

// Event handler
SDL_Event e;
SDL_Event ultimo_e;

// Event Variables
bool quit = false;

GLuint gProgramID;

GLuint gVBO;
GLuint gIBO;

GLint gVertexPos2DLocation;

bool gRenderQuad = true;


void printProgramLog( GLuint program )
{
    //Make sure name is shader
    if( glIsProgram( program ) )
    {
        //Program log length
        int infoLogLength = 0;
        int maxLength = infoLogLength;
        
        //Get info string length
        glGetProgramiv( program, GL_INFO_LOG_LENGTH, &maxLength );
        
        //Allocate string
        char* infoLog = new char[ maxLength ];
        
        //Get info log
        glGetProgramInfoLog( program, maxLength, &infoLogLength, infoLog );
        if( infoLogLength > 0 )
        {
            //Print Log
            printf( "%s\n", infoLog );
        }
        
        //Deallocate string
        delete[] infoLog;
    }
    else
    {
        printf( "Name %d is not a program\n", program );
    }
}

void printShaderLog( GLuint shader )
{
    //Make sure name is shader
    if( glIsShader( shader ) )
    {
        //Shader log length
        int infoLogLength = 0;
        int maxLength = infoLogLength;
        
        //Get info string length
        glGetShaderiv( shader, GL_INFO_LOG_LENGTH, &maxLength );
        
        //Allocate string
        char* infoLog = new char[ maxLength ];
        
        //Get info log
        glGetShaderInfoLog( shader, maxLength, &infoLogLength, infoLog );
        if( infoLogLength > 0 )
        {
            //Print Log
            printf( "%s\n", infoLog );
        }

        //Deallocate string
        delete[] infoLog;
    }
    else
    {
        printf( "Name %d is not a shader\n", shader );
    }
}

bool init() {
  bool success = true;
  // Initialize SDL
  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
    success = false;
  } else {
    // Set texture filtering to linear
    if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1")) {
      printf("Warning: Linear texture filtering not enabled!");
    }

    // Create window
    gWindow =
        SDL_CreateWindow("Test Opengl", SDL_WINDOWPOS_UNDEFINED,
                         SDL_WINDOWPOS_UNDEFINED, WIDTH, HEIGHT, WindowFlags);
    if (gWindow == NULL) {
      printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
      success = false;
    } else {
      // Use OpenGL 3.3 core
      SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
      SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
      SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
                          SDL_GL_CONTEXT_PROFILE_CORE);

      gContext = SDL_GL_CreateContext(gWindow);
      if (gContext == NULL) {
        printf("OpenGL context could not be created! SDL Error: %s\n",
               SDL_GetError());
        success = false;
      } else {
        // Initialize GLEW
        glewExperimental = GL_TRUE;
        GLenum glewError = glewInit();
        if (glewError != GLEW_OK) {
          printf("Error initializing GLEW! %s\n",
                 glewGetErrorString(glewError));
        }
        else{
          printf("GLEW initialized!\n");
        }
        // Use Vsync
        if (SDL_GL_SetSwapInterval(1) < 0) {
          printf("Warning: Unable to set VSync! SDL Error: %s\n",
                 SDL_GetError());
        }
        else{
          printf("Using Vsync!\n");
        }

        // Initialize OpenGL
        gProgramID = glCreateProgram();
        int nrAttributes;
        glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &nrAttributes);
        std::cout << "Maximum nr of vertex attributes supported: " << nrAttributes << std::endl;
      }
    }
  }

  return success;
}

void handleEvents(){
  //gerenciador de eventos
				while (SDL_PollEvent(&e) != 0)
				{
					//sair
					if (e.type == SDL_QUIT)
					{
						quit = true;
					}
					if (e.type == SDL_KEYDOWN){
						// grava o estado de uma flecha
						if(e.key.keysym.sym == SDLK_SPACE){

						}
						if(e.key.keysym.sym == SDLK_ESCAPE){
							quit = true;
						}
					}
					if(e.type == SDL_MOUSEBUTTONUP){
            // do something
					}
					if(e.type == SDL_MOUSEBUTTONDOWN){
            // do something
					}
				}
}


uint genTexture(const char* path){
  uint texture;
  glGenTextures(1, &texture);
  glBindTexture(GL_TEXTURE_2D, texture);
  // set the texture wrapping/filtering options (on the currently bound texture object)
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  // load and generate the texture
  int width, height, nrChannels;
  unsigned char *data = stbi_load(path, &width, &height, &nrChannels, 0);
  if (data)
  {
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
      glGenerateMipmap(GL_TEXTURE_2D);
  }
  else
  {
      std::cout << "Failed to load texture" << std::endl;
  }
  stbi_image_free(data);
  return texture;
};

int main(int ArgCount, char **Args) {

  init();

  alv::UIcore* uiBase = new alv::UIcore();
  alv::UIbox* box = new alv::UIbox();
  alv::UIloadingBar* bar = new alv::UIloadingBar();
  alv::UItext* text = new alv::UItext("bla");
  std::cout << "comecou UI" << std::endl;
  box->add(bar);
  std::cout << "Adicionou barra à box" << std::endl;
  box->add(text);
  std::cout << "Adicionou text à box" << std::endl;
  uiBase->add(box);
  std::cout << "Adicionou box à base" << std::endl;
  uiBase->render();
  std::cout << "finalizou UI" << std::endl;


  unsigned int iteration = 0;
  float scale = -0.02;

  while(!quit){
    handleEvents();

    uiBase->render();

    SDL_GL_SwapWindow(gWindow);

  }

  SDL_Quit();
  
  return 0;
}