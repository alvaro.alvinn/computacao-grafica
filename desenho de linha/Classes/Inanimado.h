#ifndef INANIMADO_H_INCLUDED
#define INANIMADO_H_INCLUDED
#include "Vetor2D.h"


using namespace std;

class Inanimado {
protected:
    Vetor2D posicao;
    bool visivel;

public:
    Inanimado(double posX, double posY);
    Inanimado();
    
    Vetor2D getPosicao();
    void setPosicao(double x, double y);
    void setPosicao(Vetor2D vet);

    bool getVisivel();
    void setVisivel(bool v);
};

Inanimado::Inanimado(double posX, double posY) {
    posicao = Vetor2D(posX, posY);
    visivel = false;
}

Inanimado::Inanimado() {
    posicao = Vetor2D();
    visivel = false;
}

Vetor2D Inanimado::getPosicao() {
    return posicao;
}

void Inanimado::setPosicao(double x, double y) {
    posicao = Vetor2D(x, y);
}

void Inanimado::setPosicao(Vetor2D vet) {
    posicao = vet;
}

bool Inanimado::getVisivel() {
    return visivel;
}

void Inanimado::setVisivel(bool v) {
    visivel = v;
}

#endif // CLASSES_H_INCLUDED

