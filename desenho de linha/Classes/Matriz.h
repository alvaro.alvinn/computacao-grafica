#ifndef MATRIZ_H_INCLUDED
#define MATRIZ_H_INCLUDED

#define PI 3.14159265

//include "Vetor2D.h"
#include <iostream>
#include <cmath>
class Vetor2D;

#include <vector>

using namespace std;



//Armazena uma matriz (linhas, colunas)
template <typename T> class Matriz {
private:
    //linhas
    unsigned linhas_;
    //colunas
    unsigned colunas_;
    //vetor com os dados da matriz
    vector<T> dado_;

public:
    //construtor padrao tudo = 0
    Matriz();

    //constutor que recebe outra matriz m
    Matriz(const Matriz & m);

    //matriz recebe outra matrix
    Matriz& operator=(const Matriz & m);

    //retorna o valor da posição (inicia com 0,0)
    Matriz(unsigned linhas, unsigned cols);

    // cria uma matriz formad por dois vetores [v1,v2]
    Matriz(Vetor2D v1, Vetor2D v2);

    //printa a matriz
    void print();
    // não funciona:
    //friend std::ostream& operator<<(std::ostream& os, const Matriz &);

    //retorna o devido a posição do valor seliconado (para ser atribuido com o =)
    T& operator()(unsigned linha, unsigned coluna);

    //retorna o valor da posição
    T operator()(unsigned linha, unsigned coluna) const;

    //retorna a matriz resultada da soma com outra matriz m
    Matriz operator+(const Matriz & m);

    //retorna a matriz resultada da subtração com outra matriz m
    Matriz operator-(const Matriz & m);

    //retorna a matriz resultada da multiplicação por um numero real
    Matriz operator*(const int n);

    //retorna a matriz resultada da multiplicação de outra matriz m
    Matriz operator*(const Matriz & m);

    //retorna se a matriz é igual à comparada
    bool operator==(Matriz<T> & m) const;

    //retorna se a matriz é diferente à comparada
    bool operator!=(Matriz<T> & m) const;

    //inicia a Matriz com um array
    void operator=(vector<T> v);

    //tansforma matriz em uma matriz identidade 
    int to_identidade();

    //retoran um vetor2d da matriz;
    Vetor2D vetor();

    //retorna quantidade de linhas
    unsigned getLinhas();

    //determina quantidade de linhas
    void setLinhas(unsigned lin);

    //retorna quantidade de colunas
    unsigned getColunas();

    //determina a quantidade de colunas
    void setColunas(unsigned col);

    //obtem o vector que armazena os valores
    vector<T> getDados();

    //inicia o vector de dados
    void setDados();

    //inicia uma matriz de rotação dados os radianos
    void Matriz_Rotacao(double rad);

    // retorna a matriz transposta
    Matriz transposta();

    // retorna a resolucao do sistema liner utilizando o método de Gauss Jordan
    Matriz gaussJordan();

    // retorna a resolucao do sistema liner utilizando o método de Cholensky
    Matriz cholesky(Matriz x);

    Matriz forward(Matriz x);

    Matriz backward(Matriz x);

    //retorna a matriz inversa
    Matriz inversa();

    // retorna a matriz concatenada com outra, que é passada como argumento
    Matriz concatenar(Matriz m2);
};

template <typename T> Matriz<T>::Matriz() :
    linhas_(0), colunas_(0), dado_() {
}

template <typename T> Matriz<T>::Matriz(const Matriz & copy) : 
    linhas_(copy.linhas_), colunas_(copy.colunas_), dado_(copy.dado_)
{
}

template <typename T> Matriz<T>& Matriz<T>::operator=(const Matriz & copy)
{
    linhas_ = copy.linhas_;
    colunas_ = copy.colunas_;
    dado_ = copy.dado_;

    return *this;
}

template <typename T> Matriz<T>::Matriz(Vetor2D v1, Vetor2D v2):
linhas_(2), colunas_(2), dado_(4,0)
{
    this->operator()(0,0) = v1.x;
    this->operator()(0,1) = v1.y;
    this->operator()(1,0) = v2.x;
    this->operator()(1,1) = v2.y;
}

template <typename T> Matriz<T>::Matriz(unsigned linhas, unsigned cols) :
    linhas_(linhas), colunas_(cols), dado_(linhas*cols,0)
{
}

template <typename T> T& Matriz<T>::operator()(unsigned linha, unsigned coluna) {
    return dado_[colunas_ * linha + coluna];
}

template <typename T> T Matriz<T>::operator()(unsigned linha, unsigned coluna) const {
    return dado_[colunas_ * linha + coluna];
}


template <typename T> void Matriz<T>::print()
{   
    int i, j;
    for(i=0; i<linhas_;i++){
        cout << endl;
        for(j=0; j< colunas_; j++){
            cout << operator()(i,j) << "  ";
        }
    }
    cout << endl;
}

//emplate <typename T> Matriz<T>& Matriz<T>::operator<<(std::ostream& os) { os << *this; return *this; }

template <typename T> std::ostream& operator<<(std::ostream &os, Matriz<T>&)
{
    int i, j;
    for(i=0; i< Matriz<T>::getLinhas();i++){
        os << endl;
        for(j=0; j< Matriz<T>::getColunas(); j++){
            os << Matriz<T>::operator()(i,j) << "  ";
        }
    }
    return os;
}

template <typename T> Matriz<T> Matriz<T>::operator+(const Matriz & m){
    int i;
    Matriz m1(linhas_, colunas_);
    //percorre as matrizes e vai atribuindo o resultado à nova matriz
    for(i=0; i <= colunas_ * (linhas_ -1) + (colunas_ -1) ;i++ ){
        m1.dado_[i]= dado_[i] + m.dado_[i];
    }
    return m1;
}

template <typename T> Matriz<T> Matriz<T>::operator-(const Matriz<T> & m){
    int i;
    Matriz m1(linhas_, colunas_);
    //percorre as matrizes e vai atribuindo o resultado à nova matriz
    for(i=0; i <= colunas_ * (linhas_ -1) + (colunas_ -1) ;i++ ){
        m1.dado_[i]= dado_[i] - m.dado_[i];
    }
    return m1;
}

template <typename T> Matriz<T> Matriz<T>::operator*(const int n){
    int i;
    Matriz m1(linhas_, colunas_);
    //percorre as matrizes e vai atribuindo o resultado à nova matriz
    for(i=0; i <= colunas_ * (linhas_ -1) + (colunas_ -1) ;i++ ){
        m1.dado_[i]= dado_[i] * n;
    }
    return m1;
}

template <typename T> Matriz<T> Matriz<T>::operator*(const Matriz & m){
    if(colunas_ != m.linhas_){
        cout << "erro ao multiplicar matrizes, colunas da primeira nao confere com linhas da segunda" << endl;
        return Matriz(0,0);
    }
    else{
        int i, j, k;
        Matriz m1(linhas_, m.colunas_);
            for(i = 0; i < linhas_; i++){
                for(j=0; j < colunas_; j++){
                    for(k=0; k<m.colunas_; k++){
                        m1(i,k) = m1(i,k) + operator()(i,j) * m(j,k);
                    }
                }
            }
        return m1;
    }
}

template <typename T> bool Matriz<T>::operator==(Matriz<T> & m) const{
    return(this->dado_ == m.getDados());
}

template <typename T> bool Matriz<T>::operator!=(Matriz<T> & m) const{
    return(this->dado_ != m.getDados());
}

template <typename T> void Matriz<T>::operator=(vector<T> v){
    if(v.size() == colunas_*linhas_){
        for(int i=0; i<linhas_; i++)
            for(int j=0; j<colunas_; j++)
                    operator()(i,j)= v[i*colunas_+j];
    }
    else{
        printf("vector incompativel com matriz");
    }

}

template <typename T> int Matriz<T>::to_identidade(){
    int i, j;
    if(colunas_ != linhas_){
        cout << "matriz não quadrada impossiver tranformar em identidade" << endl;
        return 0;
    }
    else{
        for(i=0; i< linhas_; i++){
            for(j=0; j < colunas_; j++){
                if(i == j){
                    operator()(i,j) = 1;
                }
                else{
                    operator()(i,j) = 0;
                }
            }
        }
    return 1;
    }
}
/* TODO: ADD TEMPLATE TO VETOR2D
template <typename T> Vetor2D Matriz<T>::vetor(){
    if(linhas_ != 2 && colunas_ != 1){
        cout << "impossivel converter para vetor tamaho nao comportado!" << endl;
        return Vetor2D();
    }
    else{
        return Vetor2D<T>(operator()(0,0), operator()(1,0));
    }
}
*/

template <typename T> unsigned Matriz<T>::getLinhas(){
    return linhas_;
}

template <typename T> void Matriz<T>::setLinhas(unsigned lin){
    linhas_ = lin;
}

template <typename T> unsigned Matriz<T>::getColunas(){
    return colunas_;
}

template <typename T> void Matriz<T>::setColunas(unsigned col){
    colunas_ = col;
}

template <typename T> vector<T> Matriz<T>::getDados(){
    return dado_;
}

template <typename T> void Matriz<T>::setDados(){
    dado_ = vector<T>(linhas_ * colunas_, 0);
}

template <typename T >void Matriz<T>::Matriz_Rotacao(double rad){
    if(linhas_ == 3 || colunas_ == 3){
        operator()(0,0) = cos(rad);
        operator()(0,1) = -sin(rad);
        operator()(0,2) = 0;
        operator()(1,0) = sin(rad);
        operator()(1,1) = cos(rad);
        operator()(1,2) = 0;
        operator()(2,0) = 0;
        operator()(2,1) = 0;
        operator()(2,2) = 1;
        return;
    }
    else if(linhas_ == 2 || colunas_ == 2){
        operator()(0,0) = cos(rad);
        operator()(0,1) = -sin(rad);
        operator()(1,0) = sin(rad);
        operator()(1,1) = cos(rad);
        return;
    }
    else{
        printf("Matriz de rotação precisa ser formada a paritir de 2x2 ou 3x3!\n");
        return;
    }
}

template <typename T> Matriz<T> Matriz<T>::transposta(){
    Matriz<T> t(colunas_, linhas_);
    for(int i = 0; i<linhas_; i++){
        for(int j = 0; j<colunas_; j++){
            t(j,i) = operator()(i,j);
        }
    }
    return t;
}

template <typename T> Matriz<T> Matriz<T>::gaussJordan(){
    int i, j, k;
    Matriz<T> x(linhas_, 1);
    double b;
    Matriz<T> a = *this;
    //percorre a matriz coluna por colunas
    for(j=0; j<linhas_; j++) {
        for(i=0; i<linhas_; i++) {
            //caso não seja a dicagonal principal
            if(i!=j) {
                //determina o divisor para que o que esta abaixo/a cima do pivo seja zerado
                b = a(i,j) / a(j,j);
                for(k=0; k<colunas_; k++) {
                    #ifdef DEBUG
                    double aux = a(i,k); 
                    //tranforma toda a linhas para que a matriz continue consistente 
                    #endif
            
                    a(i,k) = (a(i,k) - (b * a(j,k)));
                    #ifdef DEBUG
                        printf("[%d,%d] -> %.2f = %.2f - (%.2f * %.2f) \n", i, k, a(i,k), aux, b, a(j,k)); 
                    #endif
                }
            //cout << endl;
            //a.print();
            //cout << "===============================================" << endl;
            }
        }
    }
    //a.print();
    //armazena os resultados em uma matriz
    for(i=0; i<linhas_; i++) {
        x(i,0)= (a(i,colunas_-1) / a(i,i));
        #ifdef DEBUG
            printf("%.0f = %.2f / %.2f\n", x(i,0), a(i, colunas_-1), a(i,i));
            cout<<"x"<< i << "=" << x(i,0)<<" ";
        #endif
    }
    return x;
}

template <typename T> Matriz<T> Matriz<T>::cholesky(Matriz<T> x){

    if(x.linhas_ != linhas_ || x.colunas_ != 1){
        std::cout << "x não corresponde ao numero de linhas da matriz ou não possui somente uma coluna!" << std::endl;
        Matriz<T> retorno(1,1);
        return retorno;
    }
    Matriz<T> mG(linhas_, colunas_);
    int i, j ,k;
    
    for(j = 0; j<linhas_; j++){
        for(i = j; i<colunas_; i++){
            //std::cout << i << j << std::endl;
            if(i == 0 && j == 0){
                #ifdef DEBUG
                    std::cout << "G[" << i << j << "] = " << "sqrt(" << this->operator()(i,j) << ")";
                #endif
                mG(i,j) = sqrt(this->operator()(i,j));
                #ifdef DEBUG
                    std::cout << " = " << mG(i,j) << std::endl;
                #endif
            }
            else if(j == 0){
                #ifdef DEBUG
                    std::cout << "G[" << i << j << "] = " << this->operator()(i,j) << "/" << mG(0,0);
                #endif
                mG(i,j) = this->operator()(i,j)/mG(0,0);
                #ifdef DEBUG
                    std::cout << " = " << mG(i,j) << std::endl;
                #endif
            }
            else if(i == j){
                T delta = 0;
                
                for(k = 0; k<i; k++){
                    delta += mG(i,k)*mG(i,k);
                    #ifdef DEBUG
                        std::cout << "  " << delta << " = " << mG(i,k) << " * " << mG(i,k) << std::endl;
                    #endif
                }
                #ifdef DEBUG
                    std::cout << "G[" << i << j << "] = "<< "sqrt(" << this->operator()(i,j) << "-" << delta << ")";
                #endif
                mG(i,j) = sqrt(this->operator()(i,j) - delta);
                #ifdef DEBUG
                    std::cout << " = " << mG(i,j) << std::endl;
                #endif
            }
            else{
                T delta = 0;
                
                    for(k = 0; k<j; k++){
                        delta += mG(i,k)*mG(j,k);
                        #ifdef DEBUG
                            std::cout << "  " << delta << " = " << mG(i,k) << " * " << mG(j,k) << std::endl;
                        #endif
                    }
                #ifdef DEBUG
                    std::cout << "G[" << i << j << "] = "<< "sqrt(" << this->operator()(i,j) << "-" << delta << ")/" << mG(j,j);
                #endif
                mG(i,j) = (this->operator()(i,j) - delta)/mG(j,j);
                #ifdef DEBUG
                    std::cout << " = " << mG(i,j) << std::endl;
                #endif
            }
        }
    }

    mG.print();
    std::cout << "y:" << std::endl;
    Matriz<T> y =  mG.forward(x);
    y.print();
    
    std::cout << "resultado:" << std::endl;
    return mG.transposta().backward(y);
}

template <typename T> Matriz<T> Matriz<T>::forward(Matriz<T> x){
    if(x.linhas_ != linhas_ || x.colunas_ != 1){
        std::cout << "x não corresponde ao numero de linhas da matriz ou não possui somente uma coluna!" << std::endl;
        Matriz<T> retorno(1,1);
        return retorno;
    }
    Matriz<T> resposta(linhas_, 1);
    //percorre a matriz coluna por colunas
    for(int i = 0; i<linhas_; i++){
        T delta = 0;
        for(int j = 0; j<i; j++){
            delta += this->operator()(i,j) * resposta(j,0);
            //std::cout << "  " << this->operator()(i,j) << " * " << resposta(j,0) << std::endl;
        }
        resposta(i,0) = (x(i,0) - delta)/this->operator()(i,i);
        //std::cout << "resposta: " << "(" << x(i,0) << " - " << delta << ")/" << this->operator()(i,i) << " = " << resposta(i,0) << std::endl;

    }
    return resposta;
}

template <typename T> Matriz<T> Matriz<T>::backward(Matriz<T> x){
    if(x.linhas_ != linhas_ || x.colunas_ != 1){
        std::cout << "x não corresponde ao numero de linhas da matriz ou não possui somente uma coluna!" << std::endl;
        Matriz<T> retorno(1,1);
        return retorno;
    }
    Matriz<T> resposta(linhas_, 1);
    //percorre a matriz coluna por colunas
    for(int i = linhas_ - 1; i>=0; i--){
        T delta = 0;
        for(int j = linhas_ - 1; j>i; j--){
            delta += this->operator()(i,j) * resposta(j,0);
            //std::cout << "  " << this->operator()(i,j) << " * " << resposta(j,0) << std::endl;
        }
        resposta(i,0) = (x(i,0) - delta)/this->operator()(i,i);
        //std::cout << "resposta: " << "(" << x(i,0) << " - " << delta << ")/" << this->operator()(i,i) << " = " << resposta(i,0) << std::endl;
    }
    return resposta;
}

template <typename T> Matriz<T> Matriz<T>::inversa(){
    int i, j, k;
    Matriz<T> identidade(linhas_, colunas_);
    identidade.to_identidade();
    Matriz<T> x(linhas_, colunas_);
    double b;
    Matriz<T> a = *this;

    a = a.concatenar(identidade);

    //percorre a matriz coluna por colunas
        for(j=0; j<linhas_; j++) {
            for(i=0; i<a.linhas_; i++) {
                //caso não seja a diagonal principal
                if(i!=j) {
                    //determina o divisor para que o que esta abaixo/a cima do pivo seja zerado
                    b = a(i,j) / a(j,j);
                    for(k=0; k<a.colunas_; k++) {
                    #ifdef DEBUG
                    double aux = a(i,k); 
                    //tranforma toda a linhas para que a matriz continue consistente 
                    #endif
                    a(i,k) = a(i,k) - (b * a(j,k));
                    #ifdef DEBUG
                     printf("[%d,%d] -> %.2f = %.2f - %.2f / %.2f \n", i, k, a(i,k), aux, b, a(j,k));
                    #endif
                    }
                //cout << endl;
                }
            }
        }

    //divide as linhas para obter a matriz inversa 
    for(i=0; i< a.linhas_; i++) {
        for(j=i; j< a.colunas_; j++){
            a(i,j) = a(i,j)/a(i,i);
            //separa na matriz auxiliar a parte da inversa
            if(j > (a.colunas_ - identidade.colunas_ - 1)){
                x(i,j - (a.colunas_ - identidade.colunas_)) = a(i,j);
            }
        }
    }
    //cout << a << endl;
    return x;

}

template <typename T> Matriz<T> Matriz<T>::concatenar(Matriz<T> m2){
    //cria matriz auxiliar
    Matriz<T> nova_m(linhas_, colunas_ + m2.colunas_);
    //é preciso que as matrizes tenham a mesma quantidade de colunas
    if(linhas_ == m2.linhas_){
        //copia a primeira matriz
        for(int i = 0; i<linhas_; i++){
            for(int j = 0; j<colunas_; j++){
                nova_m(i,j) = operator()(i,j);
            }
        }
        //concatena a segunda matriz
        for(int i = 0; i<linhas_; i++){
            for(int j = colunas_; j<colunas_ + m2.colunas_; j++){
                nova_m(i,j) = m2(i,j - colunas_);
            }
        }
        return nova_m;
    }
    else{
        printf("Impossivel concatenar matrizes, numero de linhas divergem\n");
        return Matriz(linhas_, colunas_);
    }
}

template class Matriz<int>;
template class Matriz<float>;
template class Matriz<double>;

#endif // MATRIZ_H_INCLUDED

