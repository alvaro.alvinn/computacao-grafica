#ifndef MOUSE_H_INCLUDED
#define MOUSE_H_INCLUDED
#include <cmath>
#include "Vetor2D.h"
#include <SDL2/SDL.h>



using namespace std;

class Mouse
{
private:
	//posicao esquerda direita cima baixo
	Vetor2D posicao;

	//vetor dire��o em rela��o ao jogador
	Vetor2D direcao;


public:
	//Initializes internal variables
	Mouse();

	Vetor2D getPosicao();

	//determina a direcao
	void setPosicao(double x, double y);

	//detrmina a dire�o 
	void setDirecao(double x, double y);

	//Atualiza posicao do mouse
	void update();

	// retorna a distancia do mouse a determinado ponto
	double ditanceTo(Vetor2D position);


};

Mouse::Mouse()
{
	posicao.x = 0;
	posicao.y = 0;

	direcao.x = 0;
	direcao.y = 0;

	
}

Vetor2D Mouse::getPosicao()
{	
	Mouse::update();
	return posicao;
}

void Mouse::setPosicao(double x, double y)
{
	posicao = Vetor2D(x, y);
}

void Mouse::setDirecao(double x, double y)
{
	direcao = Vetor2D(x, y);

}



void Mouse::update() {
	int x, y;
	SDL_GetMouseState(&x, &y);
	setPosicao(x, y);
}

double Mouse::ditanceTo(Vetor2D position) {
	int x, y;
	SDL_GetMouseState(&x, &y);

	return sqrt(pow((position.x - x),2) + pow((position.y - y),2));
}




#endif // MOUSE_H_INCLUDED

#pragma once

