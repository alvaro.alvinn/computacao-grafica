#include <SDL2/SDL_render.h>
#include <iostream>
#include "Inanimado.h"
#include "Matriz.h"

class Spline : public Inanimado{
    private:
        Vetor2D* points[4];
    public:
        Spline(Vetor2D* p0, Vetor2D* p1, Vetor2D* p2, Vetor2D* p3, Vetor2D pos);
        void desenha(SDL_Renderer* r);
};

Spline::Spline(Vetor2D* p0, Vetor2D* p1, Vetor2D* p2, Vetor2D* p3, Vetor2D pos){
    points[0] = p0;
    points[1] = p1;
    points[2] = p2;
    points[3] = p3;
    posicao = pos;
    visivel = true;
}

void Spline::desenha(SDL_Renderer* r){
    float t = 0;
    Matriz<float> m1(1,4);
    Matriz<float> m2(4,4);
    m2(0,0) = 1;
    m2(1,0) = -3;
    m2(2,0) = 3;
    m2(3,0) = -1;
    m2(1,1) = 3;
    m2(2,1) = -6;
    m2(3,1) = 3;
    m2(2,2) = 3;
    m2(3,2) = -3;
    m2(3,3) = 1;
    Matriz<float> m3x(4,1);
    m3x(0,0) = points[0]->x;
    m3x(1,0) = points[1]->x;
    m3x(2,0) = points[2]->x;
    m3x(3,0) = points[3]->x;
    Matriz<float> m3y(4,1);
    m3y(0,0) = points[0]->y;
    m3y(1,0) = points[1]->y;
    m3y(2,0) = points[2]->y;
    m3y(3,0) = points[3]->y;
    //m1.print();
    //m2.print();
    //m3x.print();
    //m3y.print();

    for(t; t<=1; t = t + 0.001){
        m1(0,0) = 1;
        m1(0,1) = t;
        m1(0,2) = t*t;
        m1(0,3) = t*t*t;
        int x = (m1 * m2 * m3x)(0,0);
        int y = (m1 * m2 * m3y)(0,0);
        SDL_RenderDrawPoint(r, x + posicao.x, y+posicao.y);
    }
}