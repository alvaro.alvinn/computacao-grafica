#ifndef TEXTO_H_INCLUDED
#define TEXTO_H_INCLUDED
#include <SDL2/SDL.h>
#include "Vetor2D.h"
#include "Inanimado.h"

using namespace std;

class Texto :public Inanimado {
private:

	string conteudo;
	const char* fonte = "l_10646.ttf";
	int tamanho_fonte;
	SDL_Color cor;

public:
	Texto(string c, int posX, int posY, bool _visivel);
	Texto(string c, int posX, int posY, int tam, bool _visivel);
	Texto();

	string getConteudo();
	void setConteudo(string c);

	SDL_Color getCor();
	void setCor(SDL_Color c);

	const char* getFonte();

	int getTamanho_fonte();
	void setTamanho_fonte(int t);

	void desenha(SDL_Renderer* r);

};

Texto::Texto(string c, int posX, int posY, bool _visivel) {
	conteudo = c;

	tamanho_fonte = 20;
	posicao = Vetor2D(posX, posY);
	cor = { 255,255,255 };
	visivel = _visivel;
}

Texto::Texto(string c, int posX, int posY, int tam, bool _visivel) {
	conteudo = c;
	tamanho_fonte = tam;
	posicao = Vetor2D(posX, posY);
	cor = { 255,255,255 };
	visivel = _visivel;
}

Texto::Texto() {
	conteudo = "";
	tamanho_fonte = 20;
	posicao = Vetor2D();
	cor = { 255,255,255 };
	visivel = false;
}


string Texto::getConteudo() {
	return conteudo;
}
void Texto::setConteudo(string c ) {
	conteudo = c;
}

SDL_Color Texto::getCor() {
	return cor;
}
void Texto::setCor(SDL_Color c) {
	cor = c;
}

const char* Texto::getFonte() {
	return fonte;
}

int Texto::getTamanho_fonte() {
	return tamanho_fonte;
}
void Texto::setTamanho_fonte(int t) {
	tamanho_fonte = t;
}

void Texto::desenha(SDL_Renderer * r) {
	if (visivel) {
		// cria uma fonte com o arquivo de fonte e o tamanho detrminado no objeto texto recebido
		TTF_Font* fonte = TTF_OpenFont( this->getFonte(), tamanho_fonte);
		if (!fonte) {
			std::cout << "erro na fonte" << std::endl;
			std::cout << fonte << " " << tamanho_fonte << std::endl;
			//TTF_SetError("Loading failed :( (code: %d)", 142);
			std::cout << "Error: " << "verifique se o arquivo l_10646.ttf está localizado no mesmo diretório do executavel"  << std::endl;
			system("pause");	
			}
		else {
			// cria uma superfice com o conteudo, cor do objeto texto com a fonte recem criada
			SDL_Surface* surface = TTF_RenderText_Blended(fonte, conteudo.c_str(), cor);
			// cria uma textura coma superficie criada
			SDL_Texture* texture = SDL_CreateTextureFromSurface(r, surface);
			// variaveis que abrigaram o tamanho necessario para abrigar a textura
			int tam_x;
			int tam_y;
			// determina o tamanho ideal
			SDL_QueryTexture(texture, NULL, NULL, &tam_x, &tam_y);
			// determina onde (na tela) a textura será renderizada e o tamanho
			SDL_Rect dstrect = { (int)posicao.x, (int)posicao.y , tam_x, tam_y };
			// renderiza a textura 
			SDL_RenderCopy(r, texture, NULL, &dstrect);
			//libera espaço
			SDL_DestroyTexture(texture);
			SDL_FreeSurface(surface);
		}
		//libera o espaço alocado
		TTF_CloseFont(fonte);
	}
}




#endif // CLASSES_H_INCLUDED
