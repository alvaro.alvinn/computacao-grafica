#ifndef VETOR2D_H_INCLUDED
#define VETOR2D_H_INCLUDED

#include<cmath>
using namespace std;

class Vetor2D {
public:
    double x;
    double y;
    Vetor2D(double x, double y);
    Vetor2D();
    Vetor2D(Vetor2D* v_2);
    Vetor2D unitario();
    Vetor2D operator+(Vetor2D v_2);
    Vetor2D operator+(double num);
    Vetor2D operator-(Vetor2D v_2);
    Vetor2D operator-(double num);
    Vetor2D operator*(Vetor2D v_2);
    Vetor2D operator*(double num);
    double escalar(Vetor2D v_2);
    Vetor2D operator/(double num);
    void print();
};
Vetor2D::Vetor2D(double x, double y) {
    this->x = x;
    this->y = y;
}

Vetor2D::Vetor2D() {
    x = 0;
    y = 0;
}

Vetor2D::Vetor2D(Vetor2D *v_2) {
    x = v_2->x;
    y = v_2->y;
}
Vetor2D Vetor2D::unitario() {
       double modulo = sqrt(pow(x, 2) + pow(y, 2));
       double u_x = x / modulo;
       double u_y = y / modulo;
       return Vetor2D(u_x, u_y);
}
Vetor2D Vetor2D::operator+(Vetor2D v_2) {
    double novo_x = x + v_2.x;
    double novo_y = y + v_2.y;
    return Vetor2D(novo_x, novo_y);
}

Vetor2D Vetor2D::operator+(double num) {
    double novo_x = x + num;
    double novo_y = y + num;
    return Vetor2D(novo_x, novo_y);
}

Vetor2D Vetor2D::operator-(double num) {
    double novo_x = x - num;
    double novo_y = y - num;
    return Vetor2D(novo_x, novo_y);
}

Vetor2D Vetor2D::operator-(Vetor2D v_2) {
    double novo_x = x - v_2.x;
    double novo_y = y - v_2.y;
    return Vetor2D(novo_x, novo_y);
}

Vetor2D Vetor2D::operator*(Vetor2D v_2) {
    double novo_x = x * v_2.x;
    double novo_y = y * v_2.y;
    return Vetor2D(novo_x, novo_y);
}

Vetor2D Vetor2D::operator*(double num) {
    double novo_x = x * num;
    double novo_y = y * num;
    return Vetor2D(novo_x, novo_y);
}

double Vetor2D::escalar(Vetor2D v_2) {
    double novo_x = x * v_2.x;
    double novo_y = y * v_2.y;
    return novo_x + novo_y;
}

Vetor2D Vetor2D::operator/(double num) {
    double novo_x = x / num;
    double novo_y = y / num;
    return Vetor2D(novo_x, novo_y);
}

void Vetor2D::print(){
    printf("(%f,%f)\n", x, y);
}



#endif // CLASSES_H_INCLUDED

