// utilizando SDL, SDL image, E/S bésica, math, vector e SDL ttf.
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mouse.h>
#include <stdio.h>
#include <string>
#include <cmath>
#include <vector>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <fstream>
#include <algorithm>

// utilizando classes e funcoes criadas
#include "Classes/Vetor2D.h"
#include "Classes/Mouse.h"
#include "Classes/Texto.h"
#include "Classes/Spline.h"

#include "my_functions.h"



//Cosntantes importantes
const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;
const int RAIO_J = 25;


//Inicia o SDL e cira uma janela
bool init();

//Loads media
bool loadMedia();

//Frees media and shuts down SDL
void close();

//Loads individual image as texture
SDL_Texture* loadTexture( std::string path );

int main(int argc, char* args[]);

//The window we'll be rendering to
SDL_Window* gWindow = NULL;

//The window renderer
SDL_Renderer* gRenderer = NULL;


bool init()
{
	//Initialization flag
	bool success = true;
	if (TTF_Init() < 0) {
		printf("NÃO foi possivel inicializar SDL_TTF!, algumas funéées podem não funcionar");
	}
	//Initialize SDL
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
		success = false;
	}
	else
	{
		//Set texture filtering to linear
		if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
		{
			printf( "Warning: Linear texture filtering not enabled!" );
		}

		//Create window
		gWindow = SDL_CreateWindow( "Spline", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
		if( gWindow == NULL )
		{
			printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
			success = false;
		}
		else
		{
			//Create renderer for window
			gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
			if( gRenderer == NULL )
			{
				printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
				success = false;
			}
			else
			{
				//Initialize renderer color
				SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );

				//Initialize PNG loading
				int imgFlags = IMG_INIT_PNG;
				if( !( IMG_Init( imgFlags ) & imgFlags ) )
				{
					printf( "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
					success = false;
				}
			}
		}
	}

	return success;
}

bool loadMedia()
{
	//Loading success flag
	bool success = true;

	//Nothing to load
	return success;
}

void close()
{
	//Destroy window	
	SDL_DestroyRenderer( gRenderer );
	SDL_DestroyWindow( gWindow );
	gWindow = NULL;
	gRenderer = NULL;

	//Quit SDL subsystems
	IMG_Quit();
	TTF_Quit();
	SDL_Quit();
}

SDL_Texture* loadTexture( std::string path )
{
	//The final texture
	SDL_Texture* newTexture = NULL;

	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load( path.c_str() );
	if( loadedSurface == NULL )
	{
		printf( "Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError() );
	}
	else
	{
		//Create texture from surface pixels
        newTexture = SDL_CreateTextureFromSurface( gRenderer, loadedSurface );
		if( newTexture == NULL )
		{
			printf( "Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError() );
		}

		//Get rid of old loaded surface
		SDL_FreeSurface( loadedSurface );
	}

	return newTexture;
}


int main( int argc, char* args[] )
{
	//Start up SDL and create window
	if( !init() )
	{
		printf( "Failed to initialize!\n" );
	}
	else
	{
		//Load media
		if( !loadMedia() )
		{
			printf( "Failed to load media!\n" );
		}
		else
		{	
			//Main loop flag
			bool quit = false;

			//Event handler
			SDL_Event e;
			SDL_Event ultimo_e;

			//Instancia Mouse
			Mouse* mouse = new Mouse();

			// program variables

			Vetor2D pi(SCREEN_WIDTH/2, SCREEN_HEIGHT/2);
			Vetor2D pf(SCREEN_WIDTH/2, SCREEN_HEIGHT/2 + 100);

			Vetor2D splinePoints[4];

			Spline* spline;
			Vetor2D* selected = nullptr;

			Texto aviso = Texto("Pressione espaco para gravar flecha atual", 80, SCREEN_HEIGHT - 100, 32, true);

			int points_selected = 0;

			bool got_up = true;

			// While principal
			while (!quit)
			{	
				//gerenciador de eventos
				while (SDL_PollEvent(&e) != 0)
				{
					//sair
					if (e.type == SDL_QUIT)
					{
						quit = true;
					}
					if (e.type == SDL_KEYDOWN){
						// grava o estado de uma flecha
						if(e.key.keysym.sym == SDLK_SPACE){

						}
						if(e.key.keysym.sym == SDLK_ESCAPE){
							quit = true;
						}
					}
					if(e.type == SDL_MOUSEBUTTONUP){
						got_up = true;
						if(points_selected < 4){
							Vetor2D tmp = mouse->getPosicao() - Vetor2D(SCREEN_WIDTH/2, SCREEN_HEIGHT/2);
							splinePoints[points_selected] = new Vetor2D(tmp.x, tmp.y);							
							points_selected++;
							std::cout << "posição " << points_selected << "definida" << std::endl;
						}
						if(points_selected == 4){
							spline = new Spline(&splinePoints[0], &splinePoints[1], &splinePoints[2], &splinePoints[3], Vetor2D(SCREEN_WIDTH/2, SCREEN_HEIGHT/2));
							points_selected++;
						}
						selected = nullptr;
						
					}
					if(e.type == SDL_MOUSEBUTTONDOWN){
						got_up = false;
						if(points_selected >4){
							float menor = MAXFLOAT;
							for(int i = 0; i<4; i++){
								if(mouse->ditanceTo(splinePoints[i] + Vetor2D(SCREEN_WIDTH/2, SCREEN_HEIGHT/2)) < menor){
									menor = mouse->ditanceTo(splinePoints[i] + Vetor2D(SCREEN_WIDTH/2, SCREEN_HEIGHT/2));
									selected = &splinePoints[i];
									printf("distance: %f to point %d", menor, i);
								}
							}							
						}
					}
					if(!got_up){
						if(points_selected > 4){
								Vetor2D old_selected = Vetor2D(selected);
								Vetor2D tmp(old_selected + ((mouse->getPosicao() - Vetor2D(SCREEN_WIDTH/2, SCREEN_HEIGHT/2)) - old_selected));
								selected->x = tmp.x;
								selected->y = tmp.y;
								((mouse->getPosicao() - Vetor2D(SCREEN_WIDTH/2, SCREEN_HEIGHT/2))).print();
							}
					}
				}
				


				//limpa a tela para desenhar o próximo frame, com o fundo cinza
				SDL_SetRenderDrawColor(gRenderer, 0, 0, 0, 80);
				SDL_RenderClear(gRenderer);
				
				SDL_SetRenderDrawColor(gRenderer, 255, 255, 255, 255);
				for(int i = 0; i<4; i++){
					drawCircle(gRenderer, splinePoints[i].x + SCREEN_WIDTH/2, splinePoints[i].y + SCREEN_HEIGHT/2, 5);
					//std::cout << "desenhando circulo " << i << " em " << splinePoints[i]->x + SCREEN_WIDTH/2 << "," << splinePoints[i]->y + SCREEN_HEIGHT/2 << std::endl;
				}
				if(selected != nullptr)
					drawCircle(gRenderer, selected->x + SCREEN_WIDTH/2, selected->y + SCREEN_HEIGHT/2, 3);

				// DESENHA
				if(points_selected >= 4){
							spline->desenha(gRenderer);
				}
				
				//Atualiza a tela
				SDL_RenderPresent(gRenderer);
				
			}
		}
	}
	//Free resources and close SDL
	close();

	return 0;
}