#ifndef MYFUNCTIONS_H_INCLUDED
#define MYFUNCTIONS_H_INCLUDED

#include <SDL2/SDL.h>
#include "Classes/Matriz.h"
#include <math.h>
#include "Classes/Texto.h"

#define ROUND(a) ((int32_t)(a + 0.5))
#define RADIAN(a) ((a * PI/180))
#define DEGREES(a) ((a * 180/PI))

// Desenha contorno circulo
void drawCircle(SDL_Renderer* renderer, int32_t centreX, int32_t centreY, int32_t radius);
// Desenha preenchimento do c�rculo
int renderFillCircle(SDL_Renderer* renderer, int x, int y, int radius);
// Desenha linha entre dois pontos
void drawLine(SDL_Renderer* renderer, int32_t x0, int32_t y0, int32_t x1, int32_t y1);
void drawLine(SDL_Renderer* renderer, Vetor2D v0, Vetor2D v1);


void drawCircle(SDL_Renderer* renderer, int32_t centreX, int32_t centreY, int32_t radius)
{
	const int32_t diameter = (radius * 2);

	int32_t x = (radius - 1);
	int32_t y = 0;
	int32_t tx = 1;
	int32_t ty = 1;
	int32_t error = (tx - diameter);

	while (x >= y)
	{
		//  Each of the following renders an octant of the circle
		SDL_RenderDrawPoint(renderer, centreX + x, centreY - y);
		SDL_RenderDrawPoint(renderer, centreX + x, centreY + y);
		SDL_RenderDrawPoint(renderer, centreX - x, centreY - y);
		SDL_RenderDrawPoint(renderer, centreX - x, centreY + y);
		SDL_RenderDrawPoint(renderer, centreX + y, centreY - x);
		SDL_RenderDrawPoint(renderer, centreX + y, centreY + x);
		SDL_RenderDrawPoint(renderer, centreX - y, centreY - x);
		SDL_RenderDrawPoint(renderer, centreX - y, centreY + x);

		if (error <= 0)
		{
			++y;
			error += ty;
			ty += 2;
		}

		if (error > 0)
		{
			--x;
			tx += 2;
			error += (tx - diameter);
		}
	}
	// code by Gumichan01
}

int renderFillCircle(SDL_Renderer* renderer, int x, int y, int radius)
{
	int offsetx, offsety, d;
	int status;

	offsetx = 0;
	offsety = radius;
	d = radius - 1;
	status = 0;

	while (offsety >= offsetx) {

		status += SDL_RenderDrawLine(renderer, x - offsety, y + offsetx,
			x + offsety, y + offsetx);
		status += SDL_RenderDrawLine(renderer, x - offsetx, y + offsety,
			x + offsetx, y + offsety);
		status += SDL_RenderDrawLine(renderer, x - offsetx, y - offsety,
			x + offsetx, y - offsety);
		status += SDL_RenderDrawLine(renderer, x - offsety, y - offsetx,
			x + offsety, y - offsetx);

		if (status < 0) {
			status = -1;
			break;
		}

		if (d >= 2 * offsetx) {
			d -= 2 * offsetx + 1;
			offsetx += 1;
		}
		else if (d < 2 * (radius - offsety)) {
			d += 2 * offsety - 1;
			offsety -= 1;
		}
		else {
			d += 2 * (offsety - offsetx - 1);
			offsety -= 1;
			offsetx += 1;
		}
	}

	return status;
	// code by Gumichan01
}

void drawLine(SDL_Renderer* renderer, int32_t x0, int32_t y0, int32_t x1, int32_t y1){
	
	int dx = x1 - x0, dy = y1 - y0, steps;
	float xIncrement, yIncrement, x = x0, y = y0;
	if (abs(dx) > abs(dy)){
		steps = abs(dx);
	}
	else{
		steps = abs(dy);
	}

	xIncrement = dx / (float)steps;
	yIncrement = dy / (float)steps;

	//printf("steps: %d\n", steps);

	SDL_RenderDrawPoint(renderer, ROUND(x), ROUND(y));
	//printf("initial: %d, %d\n", ROUND(x), ROUND(y));
	for (int k = 0; k<steps; k++) {
		x += xIncrement;
		y += yIncrement;
		SDL_RenderDrawPoint(renderer, ROUND(x), ROUND(y));
	}
	//printf("final: %d, %d\n", ROUND(x), ROUND(y));
}
void drawLine(SDL_Renderer* renderer, Vetor2D v0, Vetor2D v1){
	drawLine(renderer, v0.x, v0.y, v1.x, v1.y);
}

Vetor2D rotated(Vetor2D v, double rad){
	Matriz<double> r(2,2);
	r.Matriz_Rotacao(rad);
	return Vetor2D(v.x*r(0,0)+v.y*r(1,0), v.x*r(0,1) + v.y*r(1,1));
}

void drawArrow(SDL_Renderer* renderer, int32_t x0, int32_t y0, int32_t x1, int32_t y1){
	
	int dx = x1 - x0, dy = y1 - y0, steps;
	float xIncrement, yIncrement, x = x0, y = y0;
	if (abs(dx) > abs(dy)){
		steps = abs(dx);
	}
	else{
		steps = abs(dy);
	}

	xIncrement = dx / (float)steps;
	yIncrement = dy / (float)steps;

	//printf("steps: %d\n", steps);

	SDL_RenderDrawPoint(renderer, ROUND(x), ROUND(y));
	//printf("initial: %d, %d\n", ROUND(x), ROUND(y));
	for (int k = 0; k<steps; k++) {
		x += xIncrement;
		y += yIncrement;
		SDL_RenderDrawPoint(renderer, ROUND(x), ROUND(y));
	}
	//printf("final: %d, %d\n", ROUND(x), ROUND(y));

	//printf("calculando tamanho.\n");
	
	//printf("calculando tamanho.\n");
	double size = sqrt((x1 - x0)*(x1 - x0) + (y1 - y0)*(y1 - y0));
	//double size = 10;
	size = max(size/(size*size), 5.0);
	double m = (double)(y1 - y0)/(double)(x1 - x0);

	//printf("calculando angulo.\n");

	double teta = -atan2(y1-y0, x1-x0);

	//printf("rotacionando.\n");

	//double angle0 = atan((0.0 - 0.0)/(10 - 0));
	//printf("ANGLE: %f.\n", DEGREES(angle0));

	Vetor2D l1 = rotated(Vetor2D(size,0), teta + RADIAN(135));

	Vetor2D l2 = rotated(Vetor2D(size,0), teta - RADIAN(135));

	//Vetor2D normal = rotated(Vetor2D(size+5.0,0), teta);

	//printf("TETA: %f.\n",DEGREES(teta));

	drawLine(renderer, x1, y1, (int32_t)(l1.x + x1), (int32_t)(l1.y + y1));

	drawLine(renderer, x1, y1, (int32_t)(l2.x + x1), (int32_t)(l2.y + y1));

	//Texto angulo(std::to_string(DEGREES(teta)),x1+5, y1+5, true);

	//angulo.desenha(renderer);

	//drawLine(renderer, x1, y1, (int32_t)(normal.x + x1), (int32_t)(normal.y + y1));

}
void drawArrow(SDL_Renderer* renderer, Vetor2D v0, Vetor2D v1){
	drawArrow(renderer, v0.x, v0.y, v1.x, v1.y);
}


#endif
