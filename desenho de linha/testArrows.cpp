// utilizando SDL, SDL image, E/S bésica, math, vector e SDL ttf.
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mouse.h>
#include <stdio.h>
#include <string>
#include <cmath>
#include <vector>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <fstream>
#include <algorithm>

// utilizando classes e funcoes criadas
#include "Classes/Vetor2D.h"
#include "Classes/Mouse.h"
#include "Classes/Texto.h"

#include "my_functions.h"



//Cosntantes importantes
const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;
const int RAIO_J = 25;


//Inicia o SDL e cira uma janela
bool init();

//Loads media
bool loadMedia();

//Frees media and shuts down SDL
void close();

//Loads individual image as texture
SDL_Texture* loadTexture( std::string path );

int main(int argc, char* args[]);

//The window we'll be rendering to
SDL_Window* gWindow = NULL;

//The window renderer
SDL_Renderer* gRenderer = NULL;


bool init()
{
	//Initialization flag
	bool success = true;
	if (TTF_Init() < 0) {
		printf("NÃO foi possivel inicializar SDL_TTF!, algumas funéées podem não funcionar");
	}
	//Initialize SDL
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
		success = false;
	}
	else
	{
		//Set texture filtering to linear
		if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
		{
			printf( "Warning: Linear texture filtering not enabled!" );
		}

		//Create window
		gWindow = SDL_CreateWindow( "Futebol de botoes", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
		if( gWindow == NULL )
		{
			printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
			success = false;
		}
		else
		{
			//Create renderer for window
			gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
			if( gRenderer == NULL )
			{
				printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
				success = false;
			}
			else
			{
				//Initialize renderer color
				SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );

				//Initialize PNG loading
				int imgFlags = IMG_INIT_PNG;
				if( !( IMG_Init( imgFlags ) & imgFlags ) )
				{
					printf( "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
					success = false;
				}
			}
		}
	}

	return success;
}

bool loadMedia()
{
	//Loading success flag
	bool success = true;

	//Nothing to load
	return success;
}

void close()
{
	//Destroy window	
	SDL_DestroyRenderer( gRenderer );
	SDL_DestroyWindow( gWindow );
	gWindow = NULL;
	gRenderer = NULL;

	//Quit SDL subsystems
	IMG_Quit();
	TTF_Quit();
	SDL_Quit();
}

SDL_Texture* loadTexture( std::string path )
{
	//The final texture
	SDL_Texture* newTexture = NULL;

	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load( path.c_str() );
	if( loadedSurface == NULL )
	{
		printf( "Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError() );
	}
	else
	{
		//Create texture from surface pixels
        newTexture = SDL_CreateTextureFromSurface( gRenderer, loadedSurface );
		if( newTexture == NULL )
		{
			printf( "Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError() );
		}

		//Get rid of old loaded surface
		SDL_FreeSurface( loadedSurface );
	}

	return newTexture;
}


int main( int argc, char* args[] )
{
	//Start up SDL and create window
	if( !init() )
	{
		printf( "Failed to initialize!\n" );
	}
	else
	{
		//Load media
		if( !loadMedia() )
		{
			printf( "Failed to load media!\n" );
		}
		else
		{	
			//Main loop flag
			bool quit = false;

			//Event handler
			SDL_Event e;
			SDL_Event ultimo_e;

			//Instancia Mouse
			Mouse* mouse = new Mouse();

			// program variables

			Vetor2D pi(SCREEN_WIDTH/2, SCREEN_HEIGHT/2);
			Vetor2D pf(SCREEN_WIDTH/2, SCREEN_HEIGHT/2 + 100);

			std::vector<Vetor2D*> arrows;

			Texto aviso = Texto("Pressione espaco para gravar flecha atual", 80, SCREEN_HEIGHT - 100, 32, true);


			// While principal
			while (!quit)
			{	
				//gerenciador de eventos
				while (SDL_PollEvent(&e) != 0)
				{
					//sair
					if (e.type == SDL_QUIT)
					{
						quit = true;
					}
					if (e.type == SDL_KEYDOWN){
						// grava o estado de uma flecha
						if(e.key.keysym.sym == SDLK_SPACE){
							Vetor2D *toAdd = (Vetor2D *)malloc(2 * sizeof(Vetor2D));
							toAdd[0] = pi;
							toAdd[1] = pf;
							arrows.push_back(toAdd);
						}
						if(e.key.keysym.sym == SDLK_ESCAPE){
							quit = true;
						}
					}
				}

				//limpa a tela para desenhar o próximo frame, com o fundo cinza
				SDL_SetRenderDrawColor(gRenderer, 0, 0, 0, 80);
				SDL_RenderClear(gRenderer);

				//rotaciona a linha central
				Vetor2D aux = pf - pi;
				//aux.print();
				aux = rotated(aux, RADIAN(1));
				//aux.print();
				//pi.print();
				pf = pi + aux;
				//pf.print();
				
				
				int total = arrows.size();
				// desenhas gravações
				SDL_SetRenderDrawColor(gRenderer, 150, 150, 150, 255);
				for(int i = 0; i < total; i++){
					drawArrow(gRenderer, arrows[i][0], arrows[i][1]);
				}

				// desenhas linha atual
				SDL_SetRenderDrawColor(gRenderer, 255, 255, 255, 255);				
				drawArrow(gRenderer, pi, pf);

				// DESENHA
				
				aviso.desenha(gRenderer);
				
				//Atualiza a tela
				SDL_RenderPresent(gRenderer);
				
			}
		}
	}
	//Free resources and close SDL
	close();

	return 0;
}